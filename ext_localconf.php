<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function ($extKey) {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Nng.Nnfaq',
            'Faq',
            [
                'Question' => 'categories',
            ],
            // non-cacheable actions
            [
                'Question' => 'categories',
            ]
        );
		
		\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
			'<INCLUDE_TYPOSCRIPT: source="FILE: EXT:nnfaq/Configuration/TypoScript/TSconfig/includePageTSconfig.ts">'
		);

		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		$iconRegistry->registerIcon(
			'ext-nnfaq-wizard-icon',
			\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
			['source' => 'EXT:nnfaq/Resources/Public/Icons/ce_wiz.svg']
		);

		$recordTypes = [
			'tx_nnfaq_domain_model_category',
			'tx_nnfaq_domain_model_question'
		];

		foreach ($recordTypes as $recordType) {
			$iconRegistry->registerIcon(
				'mimetypes-x-' . $recordType,
				\TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
				['source' => 'EXT:nnfaq/Resources/Public/Icons/' . $recordType . '.gif']
			);
		}

		$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1533208148] = array(
			'nodeName' => 'nnfaqCategoryTree',
			'priority' => '80',
			'class' => \Nng\Nnfaq\Form\CategoryTreeElement::class,
		);
		
		$GLOBALS ['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass']['nnfaq'] = 'Nng\Nnfaq\Hook\TCEmainHook';
		$GLOBALS ['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processCmdmapClass']['nnfaq'] = 'Nng\Nnfaq\Hook\TCEmainHook';

		/*
		if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('realurl')) {
			require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath( $extKey ) . 'Configuration/Realurl/realurl_conf.php');
		}
		*/
		
		// ------------------------------------------------------------------------------------------------
		// Externe Libraries

		require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath( $extKey ) . '/Resources/Libraries/vendor/autoload.php';

    },
    $_EXTKEY
);


