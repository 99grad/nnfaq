$(function () {

	
	$('.nnfaq-mod').each( function () {
		
		var $main = $(this);
		var $results = $main.find('.results');
		var $categoryTree, $jsTree;
		var $btnDeleteCat = $('.btn-delete');
		var $pidSelector = $main.find('.pid-selection');
		
		
		/**
		 *	Baum Konfiguration.
		 *	Root mit id=0 bestücken und immer Öffnen.
		 */ 
		function build_tree () {
		
			if (!$main.find('.tree-data').length) return;
			
			var treeData = $.parseJSON($main.find('.tree-data').val());
			//$.extend( true, treeData, {0:{type:'root', id:'0', state:{opened:true}}});
			
			if ($categoryTree) {
				$categoryTree.jstree(true).destroy();
			}

			$categoryTree = $main.find('.tree-view').jstree({
			  "core" : {
				"animation" : 0,
				"check_callback" : true,
				'data' : treeData
			  },
			  "types" : {
				"#" : {
				  "max_children" : 1,
				  "max_depth" : 10,
				  "valid_children" : ["root"]
				},
				"root" : {
				  "icon" : "fas fa-dot-circle",
				  "valid_children" : ["default", "hidden"]
				},
				"default" : {
					"icon" : "fas fa-folder-open",
					"valid_children" : ["default", "hidden"]
				},
				"hidden" : {
					"icon" : "fas fa-ban",
					"valid_children" : ["default", "hidden"]
				}
			  },
			grid: {
				columns: [
					{width:250, header: "Struktur", headerClass:'grid-header'},
					//{header: "Fragen", value: get_question_column },
					{header: "Edit", value: get_edit_column, headerClass:'grid-header'}
				]
			},
			dnd: { 
				copy:false
			},
			
			  "plugins" : [
				"ui", "dnd", "search",
				"state", "types", "wholerow", 
				"grid"
			  ]
			});
			
			$main.addClass('no-cat-selected');
			
			/**
			 * Doppelklick: Bearbeiten der Bezeichnungen
			 * Einzelklick: Fragen zu bestimmter Kategorie (inkl. Unterkategorien) laden
			 */
			var dblClicks = 0, dblClickTimer = false;
			$categoryTree.off('click.nnfaq').on('click.nnfaq','.jstree-anchor', function () {
				var instance = $.jstree.reference(this);
				var node = instance.get_node(this);
				
				dblClicks++;
				if (dblClicks == 1) {
					dblClickTimer = setTimeout( function () {
						load_questions( node.original.uid );
						dblClicks = 0;
					}, 200);
				} else {
					clearTimeout(dblClickTimer);
					dblClicks = 0;
					instance.edit(node);
				}
			});
			/*
			$categoryTree.bind("select_node.jstree", function (e, data) {
				$categoryTree.data({selected:data.selected});	
			});
			*/
			$categoryTree.bind("ready.jstree", function (e, data) {
				load_questions();
			});
			
			
			/**
			 * Nach dem Umbenennen einer Kategorie:
			 * AJAX-Request zum direkten Aktualisieren des Models
			 *
			 */
			$categoryTree.bind("rename_node.jstree", function (e, data) {
				var uid = data.node.original.uid;
				sendAjaxRequest( $main.data().updateurl, {tx_nnfaq_web_nnfaqtxnnfaqm1:{uid:uid, text:data.text}}, function () {
					load_questions( uid );
				});
			});

			
			/**
			 * Nach dem Verschieben einer Kategorie...
			 *
			 */		
			$categoryTree.bind("move_node.jstree", function (e, data) {
				var node = $(e.target).closest("li");
				var uid = data.node.original.uid;

				var ref = $categoryTree.jstree(true);
				var parent = ref.get_node(data.parent);
				sendAjaxRequest( $main.data().updateurl, {tx_nnfaq_web_nnfaqtxnnfaqm1:{uid:uid, parent:parent.original.uid, position:data.position}}, refresh_all );

				return false;
			});
		
			/**
			 * Bei hover/hoverout über eine Kategorie: uid merken, 
			 * für Drag&Drop einer Frage auf eine Kategorie im Baum.
			 *
			 */		
			$categoryTree.bind("hover_node.jstree", function (e, data) {
				var node = $(e.target).closest("li");
				var uid = data.node.original.uid;
				gHoveredCategory = uid;
				return false;
			});
		
			$categoryTree.bind("dehover_node.jstree", function (e, data) {
				gHoveredCategory = false;
			});
		
			$jsTree = $categoryTree.jstree(true);
		}		
				
		build_tree();
		
		$(document).on('click', '.btn-create', create_node );
		$(document).on('click', '.btn-delete', delete_node );
		
		/**
		 * Callback beim Rendern der Spalten für "Anzahl Fragen" ...
		 *
		 */	
		function get_question_column (node) {
			return node.original.questions;
		}

		/**
		 * ... und "Bearbeiten" Icon
		 *
		 */	
		function get_edit_column (node) {
			var uid = node.original.uid;
			if (!uid) return;
			return '<a class="fas fa-pen edit-category" data-uid="'+uid+'" href="#"></a>';			
		}
				
		/**
		 * Einen neuen Node erzeugen
		 *
		 */		
		function create_node() {
			
			var	sel = $jsTree.get_selected();

			if (!sel.length) sel = $jsTree.get_node(0);
			var	node = $jsTree.get_node(sel);

			var parent = $jsTree.get_node(node.parent);
			var parentUid = parent.original ? parent.original.uid : 0;
			var pid = $pidSelector.val();
			
			sendAjaxRequest( $main.data().createurl, {tx_nnfaq_web_nnfaqtxnnfaqm1:{parent:node.original.uid, position:999, pid:pid}}, function (data) {
				sel = $jsTree.create_node(sel[0] ? sel[0] : parentUid, {uid:data.uid, questions:data.questions, type:"default"});
				if (sel) $jsTree.edit(sel);
			});

			return false;
		};
		
		/**
		 * Löschen eines Nodes
		 *
		 */
		function delete_node() {
			var	sel = $jsTree.get_selected();
			if (!sel.length) return false;
			var node = $jsTree.get_node(sel);
			
			if (node.original.questions > 0 || node.children.length) {
				if (!confirm( 'Wirklich löschen?' )) return false;
			}
			
			sendAjaxRequest( $main.data().removeurl, {tx_nnfaq_web_nnfaqtxnnfaqm1:{uid:node.original.uid}}, function () {
				$jsTree.delete_node(sel);		
				$categoryTree.data({selected:false});
				$results.empty();
			});
			return false;
		};
		
		
		/**
		 * Klick auf "Bearbeiten" einer Kategorie:
		 * Bearbeiten über das Standard TCA-Formular
		 *
		 */
		$(document).on('click', '.edit-category', function () {
			var pid = $pidSelector.val();
			var obj = {};
			obj[$(this).data().uid] = 'edit';
			var url = createBackendQuery( 
				$main.data().editurl, 
				{edit:{tx_nnfaq_domain_model_category:obj}, nnfaqCatPid:pid}
			);
			window.location.href = url;
			return false;
		});
		
		/**
		 * Klick auf "Bearbeiten" einer Frage:
		 * Bearbeiten über das Standard TCA-Formular
		 *
		 */
		$(document).on('click', '.edit-question', function () {
			edit_question($(this).data().uid);
			return false;
		});
		
		function edit_question ( uid ) {
			var obj = {};
			obj[uid] = 'edit';
			var url = createBackendQuery( 
				$main.data().editurl, 
				{edit:{tx_nnfaq_domain_model_question:obj}}
			);
			window.location.href = url;
		}

		/**
		 * Klick auf "Löschen" einer Frage:
		 *
		 */
		$(document).on('click', '.remove-question', function () {
			if (confirm('Frage wirklich löschen?')) {
				remove_question($(this).data().uid);
				$(this).closest('.question').fadeOut();
			}
			return false;
		});
				
		function remove_question ( uid ) {
			sendAjaxRequest( $main.data().removequestionurl, {tx_nnfaq_web_nnfaqtxnnfaqm1:{uid:uid}}, refresh_all );		
		}
		
		/**
		 * Ergebnisse / Fragen für eine Kategorie per AJAX laden
		 *
		 */
		function load_questions ( categoryUid, sword, func ) {
			if (!categoryUid) {
				var sel = $jsTree.get_selected();
				categoryUid = sel && sel[0] ? sel[0] : false;
			}
			
			$main.toggleClass('no-cat-selected', !(categoryUid > 0));
			
			$results.addClass('loading');
			sendAjaxRequest($main.data().resultsurl, {tx_nnfaq_web_nnfaqtxnnfaqm1:{category:categoryUid, sword:sword}}, function (data) {
				$results.html( $('.nnfaq-results', data ).html() );
				$results.removeClass('loading');
				results_loaded();
				if (func) func.apply( this, [data] );
			});
		}
		
		
		$pidSelector.change( function () {
			var pid = $(this).val();
			var url = createBackendQuery( 
				$main.data().refreshurl, 
				{tx_nnfaq_web_nnfaqtxnnfaqm1:{pid:pid}}
			);
			window.location.href = url;
		});
		
		/* -------------------------------------------------------------------- */
		
		/**
		 * AJAX Request (z.B. zum Update eines Datensatzes) senden
		 *
		 */
		function sendAjaxRequest ( tmpArr, overrideParams, func ) {
			$main.addClass('loading');
			var url = createBackendQuery(tmpArr, overrideParams);
			console.log(url);
			$.ajax(url).done( data_loaded );
			function data_loaded ( data ) {
				if (data.substr(0,1) == '{') {
					data = $.parseJSON(data);
				}
				$main.removeClass('loading');
				if (func) func.apply( this, [data] );
			}
		}
		
		/**
		 * URL generieren zum Senden eines Requests.
		 * Verwendet ein JSON-Object als "Vorlage" (tmpArr) und überschreibt es
		 * mit den gewünschten Request-Parametern (overrideParams)
		 *
		 */	
		function createBackendQuery( tmpArr, overrideParams ) {
			var reqArr = {};
			$.extend( true, reqArr, tmpArr );
			$.extend( true, reqArr.query, overrideParams );
			return reqArr.baseUrl + reqArr.path + '?' + $.param(reqArr.query);
		}
		
		
		/**
		 * Kategorien-Baum (und ausgewählten Fragenbereich) neu laden
		 *
		 */
		function refresh_all () {
			sendAjaxRequest( $main.data().refreshurl, {}, function ( data ) {
				$main.find('.tree-data').val( $('.tree-data', data).val() );
				build_tree();
			});
		}

		/**
		 * Verwaiste Einträge zeigen
		 *
		 */		
		$(document).on('click', '.show-orphans', function () {
			$results.addClass('loading');
			sendAjaxRequest($main.data().orphanssurl, {}, function (data) {
				$results.html( $('.nnfaq-results', data ).html() );
				$results.removeClass('loading');
				results_loaded();
			});
			return false;
		});
		
		/* -------------------------------------------------------------------- */
		
		var gHoveredCategory = false;
		var gQuestionIsDragged = false;
		var gCloneQuestion = false;
		
		/**
		 * Eine neue Frage zu einer Kategorie hinzufügen
		 *
		 */	
		$(document).on('click', '.btn-create-question', function () {
			var $parent = $(this).closest('.category');
			
			var parentUid = $parent.data().catuid;
			var pid = $pidSelector.val();
			
			var obj = {tx_nnfaq_web_nnfaqtxnnfaqm1:{parent: parentUid, pid:pid}};
			sendAjaxRequest( $main.data().createquestionurl, obj, function ( data ) {
				edit_question(data.uid);
			});
			return false;
		});
		
		$main.mousemove( function (e) {
			gCloneQuestion = e.ctrlKey || e.altKey || e.metaKey || e.shiftKey;
		});
		
		function results_loaded () {
			$main.find('.questions-container').sortable({
				connectWith: '.questions-container',
				appendTo: $('.typo3-fullDoc'),
				helper: 'clone',
				dropOnEmpty: true,
				items: 	'.sortable-item',
				stop:	question_sorting_changed,
				start:	question_sorting_started
			});
		}
		
		function question_sorting_started ( event, ui ) {
			$main.addClass('dragging');
			var $parent = ui.item.closest('.category');
			ui.item.data({catUid:$parent.data().catuid});
			gQuestionIsDragged = setInterval( function () {
				$main.toggleClass( 'clone', gCloneQuestion );			
			}, 100);
		}
		
		function question_sorting_changed ( event, ui ) {
		
			$main.removeClass('dragging clone');
			var $parent = ui.item.closest('.category');
			
			var position = gHoveredCategory ? (new Date()).getTime() : $parent.find('.sortable-item').index(ui.item);
			var parentUid = gHoveredCategory ? gHoveredCategory : $parent.data().catuid;
			var curCatUid = ui.item.data().catUid;
			
			clearInterval(gQuestionIsDragged);
			gQuestionIsDragged = false;
			
			var obj = {tx_nnfaq_web_nnfaqtxnnfaqm1:{
				uid: ui.item.data().uid, 
				parent: parentUid, 
				position: position
			}};
			
			if (!gCloneQuestion && parentUid != curCatUid) {
				obj.tx_nnfaq_web_nnfaqtxnnfaqm1.rmcat = ui.item.data().catUid;
				ui.item.remove();
			}
			
			sendAjaxRequest( $main.data().updatequestionurl, obj, refresh_all);
			gHoveredCategory = false;
			//ui.item.fadeOut();
		}
		
		/* -------------------------------------------------------------------- */
		
		$('.sword').keypress( function (e) {
			if (e.keyCode == 13) {
				e.preventDefault();
				load_questions( 0, $(this).val() );
			}
		});
		
		/**
		 *	Download der Fragen als XLS-Datei
		 *
		 */ 				
		$('.btn-download').click( function () {
			var sel = $jsTree.get_selected();
			var categoryUid = sel && sel[0] ? sel[0] : false;
			var pid = $pidSelector.val();
			var sword = $('.sword').val();
			
			var url = createBackendQuery( 
				$main.data().downloadurl, 
				{tx_nnfaq_web_nnfaqtxnnfaqm1:{category: categoryUid, pid:pid, sword:sword}}
			);
			window.location.href = url;
			return false;
		});
		
		/**
		 *	Upload einer XLS-Datei
		 *
		 */ 		
		$main.find('.btn-upload').click( function () {
			$main.find('.upload-form input[type="file"]').trigger('click');
			return false;
		});
		
		$main.find('.upload-form input[type="file"]').change( function () {
			$main.find('.upload-form').submit();
		});


	});


});