/**
* Module: Nng/Nnfaq/CategoryTree
*
*/
define(['jquery'], function ($) {

	var Main = {};
	Main.init = function () {
	
		$('.tca-nnfaq-cattree').each( function () {
			$me = $(this);
			
			var $input = $me.find('.nnfaq-input');
			var $options = $me.find('.cat-item');
			
			$options.change( function () {
				var uids = [];
				$options.filter(':checked').each( function () {
					uids.push($(this).val()*1);
				});
				$input.val( uids.join(',') );
			});
		});
	};
	
	$(function() {
		Main.init();
	});
});