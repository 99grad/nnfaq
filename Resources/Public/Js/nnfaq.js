
$(function () {
	
	$('.nnfaq').each( function () {
		
		var $main = $(this);
		var $allAnswers = $main.find('.nnfaq-answer');
		var $allQuestions = $main.find('.nnfaq-question');
		var $allQuestionsAndAnswers = $main.find('.nnfaq-result-item');
		var $searchReset = $main.find('.nnfaq-searchreset');
		var $searchIcon = $main.find('.nnfaq-searchsymbol');
		var $searchField = $main.find('.nnfaq-sword');
		var $categoryDropdown = $main.find('.nnfaq-catdropdown');
		var $categoryDropdownLabel = $categoryDropdown.find('.current-cat');
		var gLastFilter = $main.find('.nnfaq-categories .active').first();

		show_hide_searchreset();
		
		/**
		 *	Sticky-Kit: Kategorien-Navigation bei Desktop fixieren, wenn gescrollt wird
		 *
		 */
		$(window).resize( function () {
			if ($(window).width() > 768) {
				make_sticky();
			} else {
				make_unsticky();
			}
		});
		function make_sticky () {
			$main.find('.col-nav').stick_in_parent({offset_top:20, sticky_class:'sticks', recalc_every: 1});
		}
		function make_unsticky () {
			$main.find('.col-nav').trigger('sticky_kit:detach');
		}
		$(window).resize();
		
		
		/**
		 *	Klick auf Frage
		 *
		 */		
		$main.find('.nnfaq-question').click( function () {
		
			var $me = $(this);
			var $item = $me.closest('.nnfaq-result-item');
			var $answer = $item.find('.nnfaq-answer');
			
			$main.find('.highlight').removeClass('highlight');
			$allQuestions.not($me).removeClass('open');
			$allAnswers.not($answer).slideUp();

			$answer.slideToggle();
			$me.toggleClass('open');

			return false;
		});

		/**
		 *	Klick auf Kategorie oder Frage: URL ändern, ohne Seite neu zu laden.
		 *	Für komfortablen, direkten Link zu einer Frage oder Kategorie.
		 *
		 */
		$main.find('.nnfaq-link-cat, .nnfaq-question').click( function () {
			var url = $(this).find('a').attr('href');
			if (!url) url = $(this).attr('href');
			if (window.history && window.history.pushState) {
				window.history.pushState("", "", url );
			}
		});
		
		/**
		 *	Klick auf Kategorie: 
		 *	Fragen dieser und aller Unterkategorien zeigen
		 */
		$main.find('.nnfaq-link-cat').click( function () {
			var $me = $(this);
			var myCatUid = $me.data().catuid;
			var cats = [myCatUid];
			
			$main.find('.active').removeClass('active');
			
			var $ref = $main.find('.nnfaq-categories [data-catuid="'+myCatUid+'"]');
			$ref.addClass('active');
			$ref.closest('li').addClass('active');
			
			$(this).parent().find('[data-catuid]').each( function (k,v){ 
				cats.push($(this).data().catuid);
			});
			
			$categoryDropdownLabel.text( $me.text() );
			filter_answers_by_category( cats );
			gLastFilter = $me;
			return false;
		});
		
		
		$categoryDropdown.find('a').click( function () {
			$categoryDropdown.removeClass('open');
		});
		
		// Wenn kein direkter Link auf Antwort: Ersten Bereich automatisch öffnen
		if (!$main.find('.highlight').length) {
			open_last_result();
		} else {
			if ($main.find('.nnfaq-questions .highlight').length) {
				$('html,body').delay(200).animate({scrollTop: $main.find('.nnfaq-questions .highlight').offset().top - 100 }, 500);
			}
		}
		
		$searchReset.click( function () {
			$searchField.val('');
			reset_all_filters();
			open_last_result();
			show_hide_searchreset();
			return false;
		});
		
		$searchField.keypress( function (e) {
			if (e.which == 13) e.preventDefault();
		});
		
		$searchField.keyup( function () {
			
			show_hide_searchreset();
			
			var keyword = $.trim( $searchField.val() );

			if (keyword.length == 0) $searchReset.click();
			if (keyword.length < 3) return false;

			filter_answers_by_keyword( keyword );
		});
		
		function mark_keyword( $el, keyword ) {
			$el.find('mark').contents().unwrap();
			var str = $el.html();
			if (!str || !str.length) return '';
			keyword = keyword.replace(/(\s+)/,"(<[^>]+>)*$1(<[^>]+>)*");
			var pattern = new RegExp("<[^>]+>|("+keyword+")", "gi");
			str = str.replace(pattern, function (p1, p2) {
				return ((p2==undefined)||p2=='')?p1:'<mark>'+p1+'</mark>';
			});
			$el.html( str );
		}
		
		
		function show_hide_searchreset() {
			var show = $searchField.val() != '';
			$searchReset.toggle( show );
			$searchIcon.toggle( !show );
		}		
		
		/**
		 *	Ergebnisse nach Keyword filtern.
		 *
		 */
		function filter_answers_by_keyword ( keyword ) {
		
			var foundInCategories = {};
			var regExp = new RegExp( keyword, 'i' );
			$main.find('.keyword-hit').removeClass('keyword-hit');
		
			$allQuestionsAndAnswers.each(function () {
            
            	var $me = $(this);
            	var $question = $me.find('.nnfaq-question');
            	var $answer = $me.find('.nnfaq-answer');

            	var foundKeyword = $me.text().search(regExp) >= 0;
            	
            	if (!foundKeyword) return;

            	foundInCategories[$me.data().catuid] = $me.data().catuid;
            	
				$me.addClass('keyword-hit');
 				$me.closest('.nnfaq-result-category').addClass('keyword-hit');
				
				mark_keyword( $question, keyword );
				mark_keyword( $answer, keyword );
				
				$question.addClass('open');
				$answer.show();
				
			});
			
			for (var uid in foundInCategories) {
 				$main.find('.nnfaq-nav [data-catuid="'+uid+'"]').addClass('keyword-hit');
			}

			$main.addClass('filter-keyword');
		}
		
		/**
		 *	Ergebnisse nach Kategorien-uids filtern.
		 *
		 */		
		function filter_answers_by_category( uid ) {
			if (!$.isArray(uid)) uid = [uid];
			var sel = [];
			$.each(uid, function (k,v) {
				sel.push('[data-catuid="'+v+'"]');
			});
			$main.find('.category-hit').removeClass('category-hit');
			$main.find(sel.join(',')).addClass('category-hit');
			$main.addClass('filter-category');
		}
		
		/**
		 *	Alle Filter zurücksetzen, alle Antworten schließen
		 *
		 */	
		function reset_all_filters () {
			$main.removeClass('filter-category filter-keyword');
			$main.find('.keyword-hit').removeClass('keyword-hit');
			$main.find('.category-hit').removeClass('category-hit');
			$main.find('.open').removeClass('open');
			$allAnswers.hide();
			$main.find('mark').contents().unwrap();
		}
		
		function open_last_result () {
			if (!gLastFilter) {
				$('.nnfaq-link-cat').first().click();
			} else {
				gLastFilter.click();
			}
		}
		
	});
	
});


/*
 Sticky-kit v1.1.2 | WTFPL | Leaf Corcoran 2015 | http://leafo.net
*/
(function(){var b,f;b=this.jQuery||window.jQuery;f=b(window);b.fn.stick_in_parent=function(d){var A,w,J,n,B,K,p,q,k,E,t;null==d&&(d={});t=d.sticky_class;B=d.inner_scrolling;E=d.recalc_every;k=d.parent;q=d.offset_top;p=d.spacer;w=d.bottoming;null==q&&(q=0);null==k&&(k=void 0);null==B&&(B=!0);null==t&&(t="is_stuck");A=b(document);null==w&&(w=!0);J=function(a,d,n,C,F,u,r,G){var v,H,m,D,I,c,g,x,y,z,h,l;if(!a.data("sticky_kit")){a.data("sticky_kit",!0);I=A.height();g=a.parent();null!=k&&(g=g.closest(k));
if(!g.length)throw"failed to find stick parent";v=m=!1;(h=null!=p?p&&a.closest(p):b("<div />"))&&h.css("position",a.css("position"));x=function(){var c,f,e;if(!G&&(I=A.height(),c=parseInt(g.css("border-top-width"),10),f=parseInt(g.css("padding-top"),10),d=parseInt(g.css("padding-bottom"),10),n=g.offset().top+c+f,C=g.height(),m&&(v=m=!1,null==p&&(a.insertAfter(h),h.detach()),a.css({position:"",top:"",width:"",bottom:""}).removeClass(t),e=!0),F=a.offset().top-(parseInt(a.css("margin-top"),10)||0)-q,
u=a.outerHeight(!0),r=a.css("float"),h&&h.css({width:a.outerWidth(!0),height:u,display:a.css("display"),"vertical-align":a.css("vertical-align"),"float":r}),e))return l()};x();if(u!==C)return D=void 0,c=q,z=E,l=function(){var b,l,e,k;if(!G&&(e=!1,null!=z&&(--z,0>=z&&(z=E,x(),e=!0)),e||A.height()===I||x(),e=f.scrollTop(),null!=D&&(l=e-D),D=e,m?(w&&(k=e+u+c>C+n,v&&!k&&(v=!1,a.css({position:"fixed",bottom:"",top:c}).trigger("sticky_kit:unbottom"))),e<F&&(m=!1,c=q,null==p&&("left"!==r&&"right"!==r||a.insertAfter(h),
h.detach()),b={position:"",width:"",top:""},a.css(b).removeClass(t).trigger("sticky_kit:unstick")),B&&(b=f.height(),u+q>b&&!v&&(c-=l,c=Math.max(b-u,c),c=Math.min(q,c),m&&a.css({top:c+"px"})))):e>F&&(m=!0,b={position:"fixed",top:c},b.width="border-box"===a.css("box-sizing")?a.outerWidth()+"px":a.width()+"px",a.css(b).addClass(t),null==p&&(a.after(h),"left"!==r&&"right"!==r||h.append(a)),a.trigger("sticky_kit:stick")),m&&w&&(null==k&&(k=e+u+c>C+n),!v&&k)))return v=!0,"static"===g.css("position")&&g.css({position:"relative"}),
a.css({position:"absolute",bottom:d,top:"auto"}).trigger("sticky_kit:bottom")},y=function(){x();return l()},H=function(){G=!0;f.off("touchmove",l);f.off("scroll",l);f.off("resize",y);b(document.body).off("sticky_kit:recalc",y);a.off("sticky_kit:detach",H);a.removeData("sticky_kit");a.css({position:"",bottom:"",top:"",width:""});g.position("position","");if(m)return null==p&&("left"!==r&&"right"!==r||a.insertAfter(h),h.remove()),a.removeClass(t)},f.on("touchmove",l),f.on("scroll",l),f.on("resize",
y),b(document.body).on("sticky_kit:recalc",y),a.on("sticky_kit:detach",H),setTimeout(l,0)}};n=0;for(K=this.length;n<K;n++)d=this[n],J(b(d));return this}}).call(this);


