<?php
defined('TYPO3_MODE') || die('Access denied.');


if (TYPO3_MODE === 'BE') {
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
		'Nng.' . $_EXTKEY,
		'web',
		'tx_nnfaq_m1',
		'bottom',
		[
			'Mod' => 'index,createCategory,updateCategory,removeCategory,getQuestions,updateQuestion,createQuestion,removeQuestion,exportQuestions,importQuestions,getOrphans,import',
		],
		[
			'access'	=> 'user,group',
			'icon'	  	=> 'EXT:'.$_EXTKEY.'/Resources/Public/Icons/moduleicon.svg',
			'labels'	=> 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_mod1.xlf',
			'navigationComponentId' => ''//'typo3-pagetree'
		]
	);
}
