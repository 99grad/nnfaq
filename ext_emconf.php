<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "nnfaq".
 *
 * Auto generated 20-11-2017 15:57
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Fragen + Antworten',
  'description' => 'Easy Frequently Asked Questions (FAQ) plugin. With categories and on-the-fly search.',
  'category' => 'plugin',
  'author' => 'David Bascom',
  'author_email' => 'david@99grad.de',
  'state' => 'stable',
  'uploadfolder' => false,
  'createDirs' => '',
  'clearCacheOnLoad' => 0,
  'version' => '8.7.0',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '8.7.0-8.7.99',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
  'clearcacheonload' => false,
  'author_company' => '99grad',
);

