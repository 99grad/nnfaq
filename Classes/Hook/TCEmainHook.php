<?php
namespace Nng\Nnfaq\Hook;


use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use \TYPO3\CMS\Core\Utility\ArrayUtility;

class TCEmainHook {

	/**
	 * @var \Nng\Nnfaq\Domain\Repository\CategoryRepository
	 * @inject
	 */
	protected $categoryRepository = NULL;
	
	/**
	 * @var \Nng\Nnfaq\Helper\AnyHelper
	 * @inject
	 */
	protected $anyHelper = NULL;
	
	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
	 * @inject
	 */
	protected $persistenceManager = NULL;
	
	/**
	 * @var \TYPO3\CMS\Core\Cache\CacheManager
	 * @inject
	 */
	protected $cacheManager = NULL;
	
	
	public function __construct() {
		$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$this->anyHelper = $objectManager->get('Nng\Nnfaq\Helper\AnyHelper');
		$this->persistenceManager = $objectManager->get('TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager');
		$this->cacheManager = $objectManager->get('TYPO3\CMS\Core\Cache\CacheManager');
		$this->categoryRepository = $objectManager->get('Nng\Nnfaq\Domain\Repository\CategoryRepository');
		$this->categoryRepository->ignoreHidden();
	}
	
    public function processCmdmap_preProcess($command, $table, $id, $value, \TYPO3\CMS\Core\DataHandling\DataHandler &$pObj) {
    	
    }
    public function processCmdmap_postProcess($command, $table, $id, $value, \TYPO3\CMS\Core\DataHandling\DataHandler &$pObj) {

    }
    
    /**
     *	#1 --- Wird VOR dem Schreiben in DB aufgerufen.
     *
     *	$id ist entweder 'NEW...' für neuen Datensatz oder die uid eines bestehenden Eintrags
     *	$fieldArray enthält ALLE Felder des TCA-Formulars
     *
     */
    public function processDatamap_preProcessFieldArray(array &$fieldArray, $table, $id, \TYPO3\CMS\Core\DataHandling\DataHandler &$pObj) {

    }

    /**
     *	#2
     *
     */
    public function processDatamap_postProcessFieldArray($status, $table, $id, array &$fieldArray, \TYPO3\CMS\Core\DataHandling\DataHandler &$pObj) {
		// Nur bei bereits gespeicherten Datensätzen prüfen, ob rekursion existiert...
		
    	if ($table == 'tx_nnfaq_domain_model_category' && is_numeric($id)) {
    		
    		// Alle aktuellen Kategorien holen
    		$categoryTreeByUid = $this->categoryRepository->getAllByUid();
    		
    		// Ist es ein anderer Branch als die Root?
    		if ($parentUid = $fieldArray['parent']) {
    		
    			$oldParent = $categoryTreeByUid[$id]->getParent()->getUid();
    			
    			// Gibt es eine Rekursion? Dann Wert zurücksetzen und Warnung ausgeben
    			if ($categoryTreeByUid[$id]->isParentOf( $parentUid )) {
    				$fieldArray['parent'] = $oldParent;
					$this->anyHelper->addFlashMessage ( 'Rekursions-Fehler', 'Die gewählte Überkategorie ist eine Unterkategorie der aktuellen Kategorie.', 'WARNING' );
    			} else {
    				$categoryTreeByUid[$id]->setParent( $categoryTreeByUid[$parentUid] );
    				$categoryTreeByUid[$oldParent]->removeChild( $categoryTreeByUid[$id] );
    			}
    		}

    	}

    }
    
    /**
     *	Wird nach dem LÖSCHEN eines Datensatzes aufgerufen
     *
     */
    public function processCmdmap_deleteAction($table, $id, $recordToDelete, $recordWasDeleted=NULL, \TYPO3\CMS\Core\DataHandling\DataHandler &$pObj) {
    	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($pObj, 'processCmdmap_deleteAction');
    }
    
	/**
     *	Wird nach DB Aktualisierungen aufgerufen
     *
     */    
    public function processDatamap_afterDatabaseOperations($status, $table, $id, array $fieldArray, \TYPO3\CMS\Core\DataHandling\DataHandler &$pObj) {
		//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($pObj, 'processDatamap_postProcessFieldArray');
    	if ($table == 'tx_nnfaq_domain_model_category') {
    		if (substr($id, 0, 3) == 'NEW') $id = $pObj->substNEWwithIDs[$id];
    	}
    }

    /**
     *	Wird nach ALLEN Aktualisierungen aufgerufen
     *
     */
    public function processDatamap_afterAllOperations(\TYPO3\CMS\Core\DataHandling\DataHandler &$pObj) {
    }

}