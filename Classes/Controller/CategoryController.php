<?php
namespace Nng\Nnfaq\Controller;

/***
 *
 * This file is part of the "nnfaq" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2016
 *
 ***/

/**
 * CategoryController
 */
class CategoryController extends \Nng\Nnfaq\Controller\AbstractController
{

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $categories = $this->categoryRepository->findAll();
        $this->view->assign('categories', $categories);
        
        return $this->renderView();
    }
}
