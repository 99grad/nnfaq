<?php
namespace Nng\Nnfaq\Controller;

use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * ModController
 */ 
class ModController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * @var \Nng\Nnfaq\Domain\Repository\QuestionRepository
	 * @inject
	 */
	protected $questionRepository = NULL;
	
	/**
	 * @var \Nng\Nnfaq\Domain\Repository\CategoryRepository
	 * @inject
	 */
	protected $categoryRepository = NULL;
	
	/**
	 * @var \Nng\Nnfaq\Helper\ExcelHelper
	 * @inject
	 */
	protected $excelHelper = NULL;
	
	/**
	 * @var \Nng\Nnfaq\Helper\AnyHelper
	 * @inject
	 */
	protected $anyHelper = NULL;
	
	/**
	 * @var \Nng\Nnfaq\Utilities\SettingsUtility
	 * @inject
	 */
	protected $settingsUtility = NULL;

	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
	 * @inject
	 */
	protected $persistenceManager = NULL;
		
	/**
	 * @var \TYPO3\CMS\Extbase\Configuration\BackendConfigurationManager
	 * @inject
	 */
	protected $backendConfigurationManager = NULL;

	
	/**
	* Initializes the current action
	* @return void
	*/
	protected function initializeAction() {
		$this->cObj = $this->configurationManager->getContentObject();
		$settings = $this->settingsUtility->getTsSetup(false, false, false);
		$this->modSettings = $settings['module']['tx_nnfaq'];
		$this->settings = $settings['plugin']['tx_nnfaq_faq'];
		$this->baseURL = $this->settingsUtility->getBaseUrl();
		$this->_GP = $this->request->getArguments();

		$this->categoryRepository->ignoreHidden();
		$this->questionRepository->ignoreHidden();
	}

	/**
	* Initializes the current view
	* @return void
	*/
	protected function initializeView( \TYPO3\CMS\Extbase\Mvc\View\ViewInterface $view ) {
	
		$this->view->assignMultiple([
			'settings'		=> $this->settings,
			'_GP'			=> $this->_GP,
			'baseURL'		=> $this->baseURL,
			'cObjData'		=> $this->cObj->data,
			'extPath' 		=> $this->settingsUtility->getRelExtPath(),
		]);
	}
	
	/**
	 * action list
	 *
	 * @return void
	 */
	public function indexAction() {

		$arguments = $this->request->getArguments();

		$sysFolders = $this->settingsUtility->getAccessableFaqSysFolders();
		$firstPid = $sysFolders ? $sysFolders[0]['pid'] : '';
		$curPid = $GLOBALS['BE_USER']->uc['nnfaq_pid'] ?: $firstPid;

		if ($pid = $arguments['pid']) {
			$GLOBALS['BE_USER']->uc['nnfaq_pid'] = $pid;
			$GLOBALS['BE_USER']->overrideUC();
			$GLOBALS['BE_USER']->writeUC();
			$curPid = $pid;
		}
		
		$this->categoryRepository->setStoragePid( $curPid );
		
		$this->view->assignMultiple([
			'categoryTree' 	=> $this->categoryRepository->getTree(),
			'curPid'		=> $curPid,
			'sysFolders'	=> $sysFolders,
		]);
	}

	
	/**
	 * action exportQuestions
	 *
	 * @return void
	 */
	public function exportQuestionsAction() {
		$arguments = $this->request->getArguments();
		$results = $this->getResults( $arguments );
		$this->excelHelper->exportQuestions( $results );
	}


	/**
	 * action getQuestions
	 *
	 * @return void
	 */
	public function getQuestionsAction() {

		$sysFolders = $this->settingsUtility->getAccessableFaqSysFolders();
		$firstPid = $sysFolders ? $sysFolders[0]['pid'] : '';
		$curPid = $GLOBALS['BE_USER']->uc['nnfaq_pid'] ?: $firstPid;

		$this->categoryRepository->setStoragePid( $curPid );
		
		$arguments = $this->request->getArguments();
		$results = $this->getResults( $arguments );
		$this->view->assignMultiple([
			'categoryTree' 		=> $results,
			'categoryTreeByUid' => $this->categoryRepository->getAllByUid(),
		]);
	}
	
	/**
	 * action getOrphans
	 *
	 * @return void
	 */
	public function getOrphansAction() {
		$this->view->assignMultiple([
			'results' 	=> $this->questionRepository->findOrphans()
		]);
	}
	
	/**
	 * getResults
	 *
	 * @return array
	 */
	private function getResults ( $arguments = [] ) {
		if ($sword = trim($arguments['sword'])) {
			$results = $this->questionRepository->findQuestionsWithConstraints(['sword'=>$sword]);
			$results = ['children'=>$results['questionsByCategories']];
		} else {
			if ($pid = $arguments['pid']) {
				$this->categoryRepository->setStoragePid( $pid );
			}
			$categoryTreeByUid = $this->categoryRepository->getAllByUid();
			$results = $categoryTreeByUid[$arguments['category']];
		}
		return $results;
	}
	
	/**
	 * action createCategory
	 *
	 * @return string
	 */
	public function createCategoryAction() {
	
		$category = $this->objectManager->get('Nng\Nnfaq\Domain\Model\Category');
		$arguments = $this->request->getArguments();
		
		$pid = intval($arguments['pid']);
		$parent = intval($arguments['parent']);
		$parent = $this->categoryRepository->findByUid($parent);
		
		$category->setParent($parent);
		$category->setPid($pid);
		$category->setSorting($arguments['position']);
				
		$this->categoryRepository->add($category);
		$this->persistenceManager->persistAll();
		
		return json_encode(['uid'=>$category->getUid(), 'questions'=>0]);
	}
	
	/**
	 * action createQuestion
	 *
	 * @return string
	 */
	public function createQuestionAction() {
	
		$arguments = $this->request->getArguments();
		if ($category = $this->categoryRepository->findByUid($arguments['parent'])) {

			$question = $this->objectManager->get('Nng\Nnfaq\Domain\Model\Question');
			$question->setSorting( time() );
			$question->setPid( $arguments['pid'] );

			$this->questionRepository->add($question);
			$this->persistenceManager->persistAll();	

			$category->addQuestion( $question );
			$this->categoryRepository->update($category);
			$this->questionRepository->update($question);
			$this->persistenceManager->persistAll();	

			return json_encode(['uid'=>$question->getUid()]);		
		}
		return json_encode(['error'=>1]);
	}
	
	/**
	 * action removeQuestion
	 *
	 * @return string
	 */
	public function removeQuestionAction() {
	
		$arguments = $this->request->getArguments();
		if ($question = $this->questionRepository->findByUid($arguments['uid'])) {
			$this->questionRepository->remove($question);	
		}
		$this->persistenceManager->persistAll();
		return json_encode(['success'=>1]);
	}
	
	/**
	 * action removeCategory
	 *
	 * @return string
	 */
	public function removeCategoryAction() {
	
		$arguments = $this->request->getArguments();
		if ($category = $this->categoryRepository->findByUid($arguments['uid'])) {
			$this->categoryRepository->remove($category);	
		}
		$this->persistenceManager->persistAll();
		return json_encode(['success'=>1]);
	}
	
	/**
	 * action updateCategory
	 *
	 * @return string
	 */
	public function updateCategoryAction() {
	
		$arguments = $this->request->getArguments();
		
		$this->categoryRepository->ignoreHidden();
		$category = $this->categoryRepository->findByUid($arguments['uid']);
		$categoryTreeByUid = $this->categoryRepository->getAllByUid();
		
		if ($category) {
		
			// Eltern-Element ändern, z.B. durch Verschieben in anderen Unterknoten?
			if (isset($arguments['parent'])) {
				$val = intval($arguments['parent']);
				$category->setParent( $this->categoryRepository->findByUid($val) );
			}
			
			// Text ändern?
			if (isset($arguments['text'])) {
				$val = $arguments['text'];
				$category->setCategory( $val );
			}
			
			// Reihenfolge der Children ändern?
			if (isset($arguments['position'])) {
				$pos = intval($arguments['position']);
				$parentUid = $category->getParent() ? $category->getParent()->getUid() : 0;

				$parent = $categoryTreeByUid[$parentUid];
				if ($parent->getChildren()) {
					$children = $parent->getChildren()->toArray();
					
					foreach ($children as $n=>$child) {
						if ($child->getUid() == $category->getUid()) unset($children[$n]);
					}
					$children = array_values($children);
					array_splice($children, $pos, 0, [$category]);
				
					foreach ($children as $n=>$child) {
						$child->setSorting($n);
						$this->categoryRepository->update($child);
					}
				}
			}
			
			$this->categoryRepository->update($category);
			$this->persistenceManager->persistAll();
		}
		return json_encode(['result'=>'1']);
	}
	
	
	/**
	 * action updateQuestion
	 *
	 * @return string
	 */
	public function updateQuestionAction() {
	
		$arguments = $this->request->getArguments();
		
		$this->categoryRepository->ignoreHidden();
		$category = $this->categoryRepository->findByUid($arguments['parent']);
		
		$question = $this->questionRepository->findByUid($arguments['uid']);

		if ($question && $category) {
							
			// Text ändern?
			if (isset($arguments['question'])) {
				$val = $arguments['question'];
				$question->setQuestion( $val );
			}
			
			// Reihenfolge der Fragen ändern?
			if (isset($arguments['position'])) {
				$pos = intval($arguments['position']);
				$questions = $category->getQuestions()->toArray();

				foreach ($questions as $n=>$child) {
					if ($question->getUid() == $child->getUid()) unset($questions[$n]);
				}
				$questions = array_values($questions);
				array_splice($questions, $pos, 0, [$question]);
				$category->removeAllQuestions();
				
				foreach ($questions as $n=>$child) {
					$category->addQuestion($child);
				}				
			}
			
			if (isset($arguments['rmcat'])) {
				if ($oldCategory = $this->categoryRepository->findByUid($arguments['rmcat'])) {
					$oldCategory->removeQuestion( $question );
					$this->categoryRepository->update($oldCategory);
				}
			}
			
			$this->categoryRepository->update($category);
			$this->persistenceManager->persistAll();
		}
		return json_encode(['result'=>'1']);
	}
	
	
	/**
	 * action import Excel
	 *
	 * @return string
	 */
	public function importAction() {
	
		$arguments = $this->request->getArguments();
		
		if ($file = trim($arguments['filepath'])) {
			if (!$arguments['process']) {
				$this->forward('index');
			} else {
				$importResult = $this->excelHelper->importQuestions(PATH_site.$file);
				$this->forward('index');
			}
		} else {
			$data = [];
			$namespace = key($_FILES);
			$targetFalDirectory = '1:/_temp_/';

			$this->registerUploadField($data, $namespace, 'file', $targetFalDirectory);		

			$fileProcessor = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Utility\\File\\ExtendedFileUtility');
			$fileProcessor->setActionPermissions();
			$fileProcessor->start($data);
			$fileProcessor->setExistingFilesConflictMode(\TYPO3\CMS\Core\Resource\DuplicationBehavior::REPLACE);	
	
			$result = $fileProcessor->processData();
			$importResult = false;
		
			if ($result['upload'] && ($firstFile = array_shift($result['upload']))) {
				if ($file = array_shift($firstFile)) {
					$importResult = $this->excelHelper->importQuestions(PATH_site.'fileadmin'.$file->getIdentifier(), true);
				}
			}
			if (!$importResult) {
				$this->anyHelper->addFlashMessage('Dateityp nicht erlaubt!', 'Die Datei wurde nicht auf den Server geladen.');
				$this->forward('index');
			}
		}	

		$this->view->assignMultiple([
			'importResult' 		=> $importResult,
			'result' 			=> $result,
			'file' 				=> $file,
			'categoryTreeByUid' => $this->categoryRepository->getAllByUid(),
		]);
	}
	
	
	/**
	 * Registers an uploaded file for TYPO3 native upload handling.
	 * 
	 * @param array &$data
	 * @param string $namespace
	 * @param string $fieldName
	 * @param string $targetDirectory
	 * @return void
	 */
	protected function registerUploadField(array &$data, $namespace, $fieldName, $targetDirectory = '1:/_temp_/') {
		if (!isset($data['upload'])) {
			$data['upload'] = [];
		}
		$counter = count($data['upload']) + 1;

		$keys = array_keys($_FILES[$namespace]);
		foreach ($keys as $key) {
			$_FILES['upload_' . $counter][$key] = $_FILES[$namespace][$key][$fieldName];
		}
		$data['upload'][$counter] = [
			'data' => $counter,
			'target' => $targetDirectory,
		];
	}

	
}