<?php
namespace Nng\Nnfaq\Controller;


/**
 * QuestionController
 */
class QuestionController extends \Nng\Nnfaq\Controller\AbstractController
{

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
       return 'listAction nicht mehr im Einsatz.';
    }
    
    /**
     * action list
     *
     * @return void
     */
    public function categoriesAction()
    {
    	$arguments = $this->request->getArguments();
        $results = $this->questionRepository->findQuestionsWithConstraints($this->settings);

		$category = '';
		if ($catUid = $arguments['category']) {
			$category = $this->categoryRepository->findByUid( $catUid );	
		}
		
		$question = '';
		if ($questionUid = $arguments['question']) {
			$question = $this->questionRepository->findByUid( $questionUid );	
		}
		
        $this->view->assignMultiple(array(
            'showSearchForm' 		=> intval($this->settings['showSearch']),
            'showNumberOfResults' 	=> intval($this->settings['showNumberOfResults']),
            'results' 				=> $results['questions'],
            'categoryTree' 			=> $results['flatTree'],
            'curQuestion' 			=> $question,
            'curCategory' 			=> $category,
        ));
        
        return $this->renderView();
    }
    
}
