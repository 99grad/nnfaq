<?php
namespace Nng\Nnfaq\Controller;

use \TYPO3\CMS\Core\Utility\ArrayUtility;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * AbstractController
 */
class AbstractController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
    
    /**
     * settingsUtility
     *
     * @var \Nng\Nnfaq\Utilities\SettingsUtility
     * @inject
     */
    protected $settingsUtility = null;
    
    /**
     * questionRepository
     *
     * @var \Nng\Nnfaq\Domain\Repository\QuestionRepository
     * @inject
     */
    protected $questionRepository = null;
    
     /**
     * categoryRepository
     *
     * @var \Nng\Nnfaq\Domain\Repository\CategoryRepository
     * @inject
     */
    protected $categoryRepository = null;
        

	/**
	* @var array
	*/    
    protected $settings = [];
    
	
	/**
	* @var array
	*/
	protected $_GP = [];
	

	/**
	 * Standard-Variablen für einen View (oder das Mail-Template) zurückgeben
	 *
	 * @return array
	 */
	public function getDefaultViewVariables ( $overrideArray = array() ) {
		
		$viewVars = array(
			'TSFE'				=> $GLOBALS['TSFE'],
			'gp'				=> $this->_GP,
			'currentPid'		=> $GLOBALS['TSFE']->id,
			'baseURL'			=> $this->settingsUtility->getBaseURL(),
			'domain'			=> $this->settingsUtility->getDomain(),
			'contentObjectData'	=> $this->configurationManager->getContentObject()->data
		);
		
		if ($overrideArray) {
			\TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule( $viewVars, $overrideArray );
		}
				
		return $viewVars;
	}
	
	/**
	* Initialisierung der Action. Wir vor initializeView() aufgerufen. 
	* Überschreibt $this->settings für das PlugIn, weil Werte aus settings.flexform.[name]
	* mit plugin.tx_nnfaq_faq.settings.[name] gemerged werden.
	*
	* @return void
	*/
	public function initializeAction() {

		$this->settings = $this->settingsUtility->getMergedSettings();
		$this->configuration = $this->settingsUtility->getConfiguration();
		
		$gp = (array) $this->request->getArguments();
		$gp['id'] = $GLOBALS['TSFE']->id;

		$this->_GP = $gp;
	}
	
	
	/**
	* 	Render der View unter Berücksichtigung des gewählten Templates.
	* 	Wurde kein Template im FlexForm gewählt: Fallback auf Default-Renderer
	*
	* @return string
	*/
	public function renderView () {

		$this->view->assignMultiple( $this->getDefaultViewVariables() );
		
		if ($renderingContext = $this->view->getRenderingContext()) {
			$renderingContext->setControllerName('');
		}

		if ($selectedTemplate = $this->settings['selectedTemplate']) {
			
			$viewPaths = $this->configuration['view'];
			$templateSettings = $this->settings['templates'][$selectedTemplate];
		 	ArrayUtility::mergeRecursiveWithOverrule($viewPaths, $templateSettings);

		 	if ($paths = $viewPaths['templateRootPaths']) {
		 		$this->view->setTemplateRootPaths($paths);
		 	}
		 	if ($paths = $viewPaths['partialRootPaths']) {
		 		$this->view->setPartialRootPaths($paths);
		 	}
		 	if ($paths = $viewPaths['layoutRootPaths']) {
		 		$this->view->setLayoutRootPaths($paths);
		 	}
			if ($path = $viewPaths['template']) {
				return $this->view->render($path);
			}
		}
		return $this->view->render();
	}
	
}
