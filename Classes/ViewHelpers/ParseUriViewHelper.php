<?php

namespace Nng\Nnfaq\ViewHelpers;

use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use \TYPO3\CMS\Core\Utility\GeneralUtility;

/*

	Wandelt eine URL in ein Array um:
	/typo3/index.php?M=web_NnfaqTxNnfaqM1&moduleToken=7c9454e13813868b87372f780e84733e48bbcb4e&tx_nnfaq_web_nnfaqtxnnfaqm1%5Buid%5D=%23uid%23&tx_nnfaq_web_nnfaqtxnnfaqm1%5Bparent%5D=%23parent%23&tx_nnfaq_web_nnfaqtxnnfaqm1%5Btext%5D=%23text%23&tx_nnfaq_web_nnfaqtxnnfaqm1%5Baction%5D=updateCategory&tx_nnfaq_web_nnfaqtxnnfaqm1%5Bcontroller%5D=Mod
	
*/
class ParseUriViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {


	/**
     * @var \Nng\Nnfaq\Utilities\SettingsUtility
     * @inject
     */
    protected $settingsUtility;


    /**
     * @var boolean
     */
    protected $escapeChildren = false;


    /**
     * @var boolean
     */
    protected $escapeOutput = false;
    
    
    /**
	 * Initialize arguments.
	 *
	 * @return void
	 */
	public function initializeArguments() {
		parent::initializeArguments();
    	$this->registerArgument('uri', 'string', 'URI, die geparsed werden soll', false);
	}
	
	
	/**
	 * 
	 *
	 * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception
	 * @return string Rendered tag
	 */
	public function render() {
		
		$uri = $this->arguments['uri'] ?: $this->renderChildren();

		$uriParts = parse_url($uri);
		parse_str( $uriParts['query'], $uriArr );
		
		$result = [
			'baseUrl' 	=> $this->settingsUtility->getBaseURL(),
			'path'		=> ltrim($uriParts['path'], '/'),
			'query'		=> $uriArr,
		]; 

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

}