<?php

namespace Nng\Nnfaq\ViewHelpers;

use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use \TYPO3\CMS\Core\Utility\GeneralUtility;


class JoinViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * @var boolean
     */
    protected $escapeChildren = false;


    /**
     * @var boolean
     */
    protected $escapeOutput = false;
    
    
    /**
	 * Initialize arguments.
	 *
	 * @return void
	 */
	public function initializeArguments() {
		parent::initializeArguments();
    	$this->registerArgument('arr', 'array', 'array to join', false);
    	$this->registerArgument('delimiter', 'string', 'delimiter', false, ',');
	}
	
	
	/**
	 * 
	 *
	 * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception
	 * @return string Rendered tag
	 */
	public function render() {
		
		$arr = $this->arguments['arr'] ?: $this->renderChildren();
		$delimiter = $this->arguments['delimiter'];

		if (!is_array($arr)) return $arr;
		return join($delimiter, $arr);
    }

}