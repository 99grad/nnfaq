<?php

namespace Nng\Nnfaq\ViewHelpers\Variable;

use TYPO3\CMS\Extbase\Reflection\ObjectAccess;

class IncrementViewHelper extends \Nng\Nnfaq\ViewHelpers\Variable\SetViewHelper {

	/**
	 * Initialize arguments.
	 *
	 * @return void
	 */
	public function initializeArguments() {
		parent::initializeArguments();
    	$this->registerArgument('inc', 'intval', 'Increment', false, 1);
	}
	
	/**
	 * Increase/decrease the variable in $name.
	 *
	 * @param string $name
	 * @param mixed $value
	 * @return void
	 */
	public function render() {
	
		$name = $this->arguments['name'];
		$inc = $this->arguments['inc'];

		if ($this->templateVariableContainer->exists($name)) {
			$curVal = $this->templateVariableContainer->get($name);
		} else {
			$curVal = 0;
		}
		
		$this->arguments['value'] = $curVal + $inc;
		return parent::render();
	}

}