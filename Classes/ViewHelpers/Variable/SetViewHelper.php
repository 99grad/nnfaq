<?php

namespace Nng\Nnfaq\ViewHelpers\Variable;

use TYPO3\CMS\Extbase\Reflection\ObjectAccess;

class SetViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
     * @var boolean
     */
    protected $escapeChildren = false;


    /**
     * @var boolean
     */
    protected $escapeOutput = false;
    
    
    /**
	 * Initialize arguments.
	 *
	 * @return void
	 */
	public function initializeArguments() {
		parent::initializeArguments();
    	$this->registerArgument('name', 'string', 'Name der Variablen', false);
    	$this->registerArgument('value', 'mixed', 'Wert', false);
	}
	
	/**
	 * Set (override) the variable in $name.
	 *
	 * @param string $name
	 * @param mixed $value
	 * @return void
	 */
	public function render() {
	
		$value = $this->arguments['value'] ?? $this->renderChildren();
		$name = $this->arguments['name'];
		
		if (FALSE === strpos($name, '.')) {
			if (TRUE === $this->templateVariableContainer->exists($name)) {
				$this->templateVariableContainer->remove($name);
			}
			$this->templateVariableContainer->add($name, $value);
		} elseif (1 === substr_count($name, '.')) {
			$parts = explode('.', $name);
			$objectName = array_shift($parts);
			$path = implode('.', $parts);
			if (FALSE === $this->templateVariableContainer->exists($objectName)) {
				return NULL;
			}
			$object = $this->templateVariableContainer->get($objectName);
			try {
				ObjectAccess::setProperty($object, $path, $value);
				// Note: re-insert the variable to ensure unreferenced values like arrays also get updated
				$this->templateVariableContainer->remove($objectName);
				$this->templateVariableContainer->add($objectName, $object);
			} catch (\Exception $error) {
				return NULL;
			}
		}
		return NULL;
	}

}