<?php

namespace Nng\Nnfaq\ViewHelpers;


class FlashMessagesViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {


	/**
     * @var boolean
     */
    protected $escapeChildren = false;

    /**
     * @var boolean
     */
    protected $escapeOutput = false;
    
	/**
	 * @var \Nng\Nnfaq\Helper\AnyHelper
	 * @inject
	 */
	protected $anyHelper;

    /**
     * @return string
     */	
   public function render() {
		$this->anyHelper->renderFlashMessages();
		return $this->anyHelper->clearFlashMessages();
    }
    
    
}