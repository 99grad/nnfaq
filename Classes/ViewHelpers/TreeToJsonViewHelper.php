<?php

namespace Nng\Nnfaq\ViewHelpers;

use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use \TYPO3\CMS\Core\Utility\GeneralUtility;

/*

	Tree.js erwartet ein Array in dieser Art:
	
	[
		{ "text" : "Root node", "children" : [
				{ "text" : "Child node 1" },
				{ "text" : "Child node 2" }
		]}
	]
	
*/
class TreeToJsonViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {


    /**
     * @var boolean
     */
    protected $escapeChildren = false;


    /**
     * @var boolean
     */
    protected $escapeOutput = false;
    
    
    /**
	 * Initialize arguments.
	 *
	 * @return void
	 */
	public function initializeArguments() {
		parent::initializeArguments();
    	$this->registerArgument('data', 'mixed', 'Kategorien-Baum', false);
	}
	
	
	/**
	 * 
	 *
	 * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception
	 * @return string Rendered tag
	 */
	public function render() {
		
		$data = $this->arguments['data'] ?: $this->renderChildren();
		if (!$data) return [];

		$treeDataAsArray = $this->getNodesAsArrayRecursion( $data );
		$treeDataAsArray[0]['type'] = 'root';		
		$treeDataAsArray[0]['id'] = '0';
		$treeDataAsArray[0]['state'] = ['opened' => true];
        return json_encode($treeDataAsArray);
    }

	private function getNodesAsArrayRecursion ( $branch ) {
		$arr = [];
		foreach ($branch as $uid=>$node) {
			$totalQuestions = $node->getTotalQuestions();
    		$obj = [
    			'id'		=> $node->getUid(),
    			'text'		=> $node->getCategory(), //. ($totalQuestions ? " ({$totalQuestions})" : ''),
    			'uid'		=> $node->getUid(),
    			'questions'	=> $node->getTotalQuestions(),
    			'children'	=> $this->getNodesAsArrayRecursion($node->getChildren())
    		];
    		if ($node->getHidden()) $obj['type'] = 'hidden';
    		$arr[] = $obj;
		}
    	return $arr;
	}

}