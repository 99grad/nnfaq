<?php
namespace Nng\Nnfaq\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Backend\Utility\BackendUtility;

/**
 *
 * @package nnfaq
 */
class ModUrlViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * @var boolean
     */
    protected $escapeChildren = false;

    /**
     * @var boolean
     */
    protected $escapeOutput = false;

	/**
	* @var \Nng\Nnfaq\Utilities\SettingsUtility
	* @inject
	*/
	protected $settingsUtility;

    /**
	 * Initialize arguments.
	 *
	 * @return void
	 */
	public function initializeArguments() {
		parent::initializeArguments();
    	$this->registerArgument('action', 'string', 'Welche Aktion?', false);
    	$this->registerArgument('uid', 'intval', 'uid des Datensatzes', false, 0);
    	$this->registerArgument('pid', 'intval', 'pid des Datensatzes', false, 0);
    	$this->registerArgument('table', 'string', 'DB Tabellenname des Datensatzes', false, 'tx_nnfaq_domain_model_category');
    	$this->registerArgument('defVals', 'array', 'Default-Werte', false, []);
    	$this->registerArgument('returnUrl', 'string', 'Return URL', false);
	}
		
	/**
	 * @return mixed
	 */	
	public function render() {

		$settings = $this->settingsUtility->getTsSetup(false, false, false);
		$modSettings = $settings['module']['tx_nnfaq'];
		
		$table = $this->arguments['table'];
		$uid = $this->arguments['uid'];
		$pid = $this->arguments['pid'];
		$action = $this->arguments['action'];
		$returnUrl = $this->arguments['returnUrl'] ?: GeneralUtility::getIndpEnv('REQUEST_URI');

		$defValsQueryStr = '';
		
		if ($defVals = $this->arguments['defVals']) {
			$tmp = [];
			foreach ($defVals as $k=>$v) {
				$tmp[] = "defVals[{$table}][{$k}]=".urlencode($v);
			}
			$defValsQueryStr = '&'.join('&', $tmp);
		}

		// ToDo: Hier könnte z.T. auch einfach der ViewHelper verwendet werden!
		// https://docs.typo3.org/typo3cms/CoreApiReference/ApiOverview/Examples/EditLinks/Index.html
				
		switch ($action) {

			case 'new':
				return BackendUtility::getModuleUrl( 'record_edit', [
						'returnUrl' => $returnUrl
					]) . "&edit[{$table}][{$pid}]=new&".$args.$defValsQueryStr;

			case 'edit':
				return BackendUtility::getModuleUrl( 'record_edit', [
						'returnUrl' => $returnUrl
					]) . "&edit[{$table}][{$uid}]=edit";
					
			case 'ajax_record_hide':
				return BackendUtility::getAjaxUrl( 'record_process', [
					]) . "&data[{$table}][{$uid}][hidden]=1";

			case 'ajax_record_unhide':
				return BackendUtility::getAjaxUrl( 'record_process', [
					]) . "&data[{$table}][{$uid}][hidden]=0";
					
			case 'delete':
				return BackendUtility::getAjaxUrl( 'record_process', [
					]) . "&cmd[{$table}][{$uid}][delete]=1";
				
			case 'return_url':
				return $returnUrl;
				
			case 'preview':
				return str_replace('{uid}', $uid, $this->settingsUtility->getBaseURL().$modSettings['settings']['previewUrl'] );

		}
		
	}
}