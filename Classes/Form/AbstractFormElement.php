<?php
namespace Nng\Nnfaq\Form;

 
use TYPO3\CMS\Core\Utility\GeneralUtility;	 
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;	 
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

class AbstractFormElement extends \TYPO3\CMS\Backend\Form\Element\AbstractFormElement {

	/**
	 * Function getTemplateHtml
	 *
	 * @param string $templateName templateName
	 * @param string $variables variables
	 *
	 * @return string
	 */
	public function getTemplateHtml($templateName, $variables = []) {

		$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$tempView = $objectManager->get('TYPO3\CMS\Fluid\View\StandaloneView');
		
		$templatePathAndFilename = $this->getAbsExtPath().'Resources/Backend/TCA/'.$templateName;
		
		$tempView->setTemplatePathAndFilename($templatePathAndFilename);

		$tempView->assignMultiple($variables ?? []);
		
		$tempView->assignMultiple([
			'site_root' => PATH_site
		]);

		return $tempView->render();
	}

	public function getAbsExtPath () {
		return ExtensionManagementUtility::extPath('nnfaq');
	}
	
	public function getRelExtPath () {
		return ExtensionManagementUtility::extRelPath('nnfaq');
	}
	
	public function render () {}
    
}