<?php
namespace Nng\Nnfaq\Form;

 
use TYPO3\CMS\Core\Utility\GeneralUtility;	 
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;	 
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;


class CategoryTreeElement extends \Nng\Nnfaq\Form\AbstractFormElement {

	/**
	* @var \Nng\Nnfaq\Domain\Repository\CategoryRepository
	* @inject
	*/
	protected $categoryRepository;

	/**
	* @var \Nng\Nnfaq\Helper\AnyHelper
	* @inject
	*/
	protected $anyHelper;
	
	
	public function render() {

		$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$this->categoryRepository = $objectManager->get('Nng\Nnfaq\Domain\Repository\CategoryRepository');
		$this->anyHelper = $objectManager->get('Nng\Nnfaq\Helper\AnyHelper');
		
        $resultArray = $this->initializeResultArray();

        $fieldWizardResult = $this->renderFieldWizard();
        $fieldWizardHtml = $fieldWizardResult['html'];
        $resultArray = $this->mergeChildReturnIntoExistingResult($resultArray, $fieldWizardResult, false);

		$PA = $this->data['parameterArray'];
		$config = $PA['fieldConf']['config'];
		
		$this->categoryRepository->ignoreHidden();
		$this->categoryRepository->ignoreStoragePid();
		
		if ($storagePages = $this->data['databaseRow']['pages']) {
			$storagePages = array_column($storagePages, 'uid');
			$this->categoryRepository->setStoragePid( $storagePages );
		} else if ($storagePages = $_GET['nnfaqCatPid']) {
			$this->categoryRepository->setStoragePid( $storagePages );
		}
		
		$tmp = $this->anyHelper->trimExplodeArray($PA['itemFormElValue']);
		$PA['itemFormElValue'] = array_combine( $tmp, $tmp );
		
		$categoryTree = $this->categoryRepository->getTree();
		$categoryTreeByUid = $this->categoryRepository->getAllByUid();

        $mainFieldHtml = $this->getTemplateHtml( 'CategoryTree/index.html', [
			'PA' 				=> $PA,
			'config' 			=> $config,
			'fieldWizardHtml'	=> $fieldWizardHtml,
			'categoryTree'		=> $categoryTree,
			'uid'				=> $this->data['vanillaUid'],
        ]);

		$resultArray['requireJsModules'][] = $this->getRelExtPath().'Resources/Backend/TCA/CategoryTree/script.js';
		$resultArray['stylesheetFiles'][] = $this->getRelExtPath().'Resources/Backend/TCA/CategoryTree/style.css';

        $resultArray['html'] = $mainFieldHtml;
        return $resultArray;
    }
    
    
}