<?php

namespace Nng\Nnfaq\Helper;

use TYPO3\CMS\Core\Utility\GeneralUtility;	 
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;	 
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;


class ExcelHelper {


	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
	 * @inject
	 */
	protected $persistenceManager = NULL;

	/**
	 * @var \Nng\Nnfaq\Domain\Repository\QuestionRepository
	 * @inject
	 */
	protected $questionRepository = NULL;
	
	/**
	 * @var \Nng\Nnfaq\Domain\Repository\CategoryRepository
	 * @inject
	 */
	protected $categoryRepository = NULL;
	
	/**
	 * @var \Nng\Nnfaq\Helper\AnyHelper
	 * @inject
	 */
	protected $anyHelper = NULL;
	
	
    /**
     *	Importiert Fragen & Antworten als EXCEL
     *
     */
	public function importQuestions ( $path = '', $testmode = false ) {
	
	
		$inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($path);
		$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
		$spreadsheet = $reader->load($path);
		
		$allSheets = $spreadsheet->getAllSheets();
		
		$parsedData = [];
		
		// Durch alle Sheets gehen...
		foreach ($allSheets as $curSheetNum=>$objWorksheet) {

			// Aktueller Titel des Sheets/Blattes
			$curSheetTitle = $objWorksheet->getTitle();
		
			// Anzahl Zeile und Spalten im Blatt holen
			$highestRow = $objWorksheet->getHighestRow(); // e.g. 10
			$highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
			$highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); // e.g. 5
			$colKeys = ['uid', 'question', 'answer'];
			
			$currentCategory = false;
			
			for ($row = 1; $row <= $highestRow; ++$row) {

				$parsedRow = [];

				$firstColValue = $objWorksheet->getCellByColumnAndRow(1, $row)->getValue();
				$isCategoryTitle = false;
				
				if (substr($firstColValue, 0, 1) == 'c') {
					$catUid = substr($firstColValue, 1);
					if (is_numeric($catUid)) {
						$currentCategory = $catUid;
						$isCategoryTitle = true;
					}
				}
						
				for ($col = 0; $col <= $highestColumnIndex; ++$col) {
					$cell = $objWorksheet->getCellByColumnAndRow($col+1, $row);
					$val =  $cell->getCalculatedValue();
					if (!$isCategoryTitle) {
						if (isset($colKeys[$col])) {
							$parsedRow[$colKeys[$col]] = $val;
						}
					}
				}
				
				if ($currentCategory && trim(join('', $parsedRow))) {
					$parsedRow['row'] = $row;
					$parsedRow['catUid'] = $currentCategory;
					if (!$parsedData[$currentCategory]) $parsedData[$currentCategory] = [];
					$parsedData[$currentCategory][] = $parsedRow;
				}
			}
				
		}
		
		$modifiedQuestions = [];
		$newQuestions = [];
		
		foreach ($parsedData as $catUid=>&$questions) {
			foreach ($questions as &$question) {
				if ($uid = $question['uid']) {
					if ($questionFromDB = $this->questionRepository->findByUid( $uid )) {
						
						$oldQuestion = preg_replace('/[^0-9a-zA-ZäöüÖÄÜß-]/', '', $questionFromDB->getQuestion());
						$newQuestion = preg_replace('/[^0-9a-zA-ZäöüÖÄÜß-]/', '', $question['question']);
						$oldAnswer = preg_replace('/[^0-9a-zA-ZäöüÖÄÜß-]/', '', $questionFromDB->getAnswer());
						$newAnswer = preg_replace('/[^0-9a-zA-ZäöüÖÄÜß-]/', '', $question['answer']);
						
						if ($oldQuestion != $newQuestion || $oldAnswer != $newAnswer ) {
						
							$question['_changed'] = true;
							$questionFromDB->setQuestion($question['question']);
							$questionFromDB->setAnswer($question['answer']);
							$modifiedQuestions[] = $questionFromDB;
						}
					} else {
						$question['_error'] = '404';
					}
				} else {
					if ($category = $this->categoryRepository->findByUid( $question['catUid'] )) {
						$question['_new'] = true;
						$question['category'] = $category;
						$newQuestions[] = $question;
					} else {
						$question['_error'] = '405';						
					}
				}
			}
		}
		
		// Nur Testmodus? Dann Rückgabe des Ergebnisses für Ausgabe
		if ($testmode) return $parsedData;

		// Update / Insert der Fragen
		foreach ($modifiedQuestions as $question) {
			$this->questionRepository->update( $question );
		}
		
		foreach ($newQuestions as $question) {
			$questionModel = new \Nng\Nnfaq\Domain\Model\Question();
			$questionModel->setQuestion( $question['question'] );
			$questionModel->setAnswer( $question['answer'] );
			$questionModel->addCategory( $question['category'] );
			$this->questionRepository->add( $questionModel );
		}
		
		$this->persistenceManager->persistAll();
		
	}


    /**
     *	Exportiert Fragen & Antworten als EXCEL
     *
     */
	public function exportQuestions ( $questions = [] ) {

		// https://phpspreadsheet.readthedocs.io/en/develop/topics/recipes/#add-rich-text-to-a-cell
		
		$now = new \DateTime();
		$filename = 'faq-'.$now->format('Ymd');
		$sheetname = 'Fragen';
		
		$categoryColor = [
			'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
			'startColor' => ['rgb' => 'DDDDDD']
		];
		$categoryFont = [
			'font'=>['bold'=>true, 'size'=>14]
		];
		$questionFont = [
			'font'=>['bold'=>true]
		];
		
		$objPHPExcel = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
		$objPHPExcel->setActiveSheetIndex(0);
		$activeSheet = $objPHPExcel->getActiveSheet();

		$htmlWizard = new \PhpOffice\PhpSpreadsheet\Helper\Html;
		
		$cols = [
			['label'=>'uid', 'width'=>5], 
			['label'=>'Frage', 'width'=>40], 
			['label'=>'Antwort', 'width'=>100],
		];
		
		$questionsByCategory = $this->get_flattened_questions_recursive($questions);

		$colCnt = 0;
		foreach ($cols as $field=>$colArr) {
			$colLetter = chr(65+$colCnt);
			$activeSheet->getColumnDimension($colLetter)->setWidth($colArr['width']);
			$colCnt++;
		}

		$rowCnt = 1;
		foreach ($questionsByCategory as $category) {

			$colCnt = 1;
			$rowCnt += 3;
			
			if (!$category['uid']) continue;
			
			$activeSheet->setCellValueByColumnAndRow(1, $rowCnt, 'c'.$category['uid']);
			$activeSheet->setCellValueByColumnAndRow(2, $rowCnt, $category['category']);
			$activeSheet->getStyle("A{$rowCnt}:X{$rowCnt}")
				->applyFromArray($categoryFont)
				->getFill()->applyFromArray($categoryColor);
			
			$rowCnt++;
			
			foreach ($category['questions'] as $question) {	
				$rowCnt++;
				
				//$answer = mb_convert_encoding(html_entity_decode($question->getAnswer()), 'HTML-ENTITIES', 'UTF-8');
				$answer = $question->getAnswer();
// 				$richtext = $htmlWizard->toRichTextObject($answer);
				$richtext = $answer;
				
				$activeSheet->setCellValueByColumnAndRow(1, $rowCnt, $question->getUid());	
				$activeSheet->setCellValueByColumnAndRow(2, $rowCnt, $question->getQuestion());	
				$activeSheet->setCellValueByColumnAndRow(3, $rowCnt, $richtext );	
				$activeSheet->getStyle("B{$rowCnt}")->applyFromArray($questionFont);
				$activeSheet->getStyle("A{$rowCnt}:X{$rowCnt}")
					->getAlignment()->setWrapText(true)
					->setVertical( \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP );
			}
		}
		
		$activeSheet->setTitle($sheetname);
		
		header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="'.$filename.'.xlsx"');

		$objWriter = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
		$objWriter->save('php://output');
		die();	
	}
	
	
	/**
     *	Daten für einfacheren Export aufbereiten
     *
     */
	private function get_flattened_questions_recursive ( $branch = [] ) {
	
		$flat = [];
		if (!$branch) return $flat;
		
		$questions = is_object($branch) ? $branch->getQuestions() : false;
		$children = is_object($branch) ? $branch->getChildren() : $branch['children'];
		
		if ($questions) {
			$catUid = $branch->getUid();
			$flat[$catUid] = [
				'questions' => [],
				'category' 	=> $branch->getCategory(),
				'uid' 		=> $branch->getUid(),
			];
			foreach ($branch->getQuestions() as $question) {
				$flat[$catUid]['questions'][] = $question;
			}
		}
				
		foreach ($children as $child) {
			if ($sub = $this->get_flattened_questions_recursive($child)) {
				$flat = $flat + $sub;
			}
		}
		return $flat;
	}
	
	
}
