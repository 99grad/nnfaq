<?php

namespace Nng\Nnfaq\Helper;

use \TYPO3\CMS\Core\Utility\GeneralUtility;


class AnyHelper {
	
	
	/**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     * @inject
     */
	protected $objectManager;
	
	/**
	* @var \Nng\Nnfaq\Utilities\SettingsUtility
	* @inject
	*/
	protected $settingsUtility;
	


	/* --------------------------------------------------------------- 
		Simple Validierung / Authentifizierung
	*/
	
	
	/**
	 *	Schlüssel erzeugen zur Validierung einer Abfrage
	 *
	 * @param int|string $uid
	 * @return string
	 */	
	public function createKeyForUid ( $uid ) {
		return substr(strrev(md5($uid.'99grad')), 0, 8);
	}
	
	/**
	 *	Schlüssel prüfen
	 *
	 * @param int|string $uid
	 * @param string $key
	 * @return boolean
	 */	
	public function validateKeyForUid ( $uid, $key ) {
		return self::createKeyForUid( $uid ) == $key;
	}
	
	
	
	/* ---------------------------------------------------------------
		Templating
	*/

	/**
	 *	Ein Fluid-Template rendern über den StandaloneView
	 *
	 * @param string 	$templateName
	 * @param array 	$vars
	 * @param array 	$rootPaths
	 * @return string
	 */	
	public function renderTemplate ( $templateName, $vars = [], $rootPaths = null ) {

		if (!$rootPaths) {
			$rootPaths = $this->settingsUtility->getTemplatePaths();
		}

		$view = $this->objectManager->get('TYPO3\CMS\Fluid\View\StandaloneView');
		$view->setPartialRootPaths( $rootPaths['partialRootPaths'] );
		$view->setTemplateRootPaths( $rootPaths['templateRootPaths'] );
		$view->setTemplate( $templateName );
		
		$view->assignMultiple($vars);
		
		$html = $view->render();
		return $html;
	}

	
	/**
	 *	Einen Fluid-Quelltext / String rendern
	 *
	 * @param string 	$template
	 * @param array 	$vars
	 * @param array 	$pathPartials
	 * @return string
	 */
	public function renderTemplateSource ( $template, $vars, $pathPartials = null ) {
		
		if (!$template) return '';
		
		if (strpos($template, '{namespace') === false) {
			$template = '{namespace VH=Nng\Nnezine\ViewHelpers}'.$template;
		}
		
		$view = GeneralUtility::makeInstance('Tx_Fluid_View_StandaloneView');
		$view->setTemplateSource($template);
		$view->setPartialRootPath( $pathPartials ? $pathPartials : \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nnezine').'Resources/Private/Partials/' );
		$view->assignMultiple( $vars );
		$html = $view->render();
		
		return $html;
	}
	
	
	/* --------------------------------------------------------------- 
		NOTICE, ERROR, WARNING, OK
		$this->anyHelper->addFlashMessage('title', 'Beschreibung');
	*/
	
	
	/**
	 *	Eine Flash-Message hinzufügen
	 *
	 * @var string 	$title
	 * @var string 	$text
	 * @var string 	$type
	 * @var string 	$identifier
	 * @return void
	 */
	public function addFlashMessage ( $title = '', $text = '', $type = 'OK', $identifier = 'core.template.flashMessages') {

		$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');	

		$flashMessageService = $objectManager->get(\TYPO3\CMS\Core\Messaging\FlashMessageService::class);
		$messageQueue = $flashMessageService->getMessageQueueByIdentifier( $identifier );

		$message = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Messaging\FlashMessage::class,
			$text,
			$title,
			constant('TYPO3\\CMS\\Core\\Messaging\\FlashMessage::'.$type),
			TRUE
		);

		$messageQueue->addMessage($message);
	}
	
	
	/**
	 *	Alle Flash-Messages im Queue löschen
	 *
	 * @var string 	$identifier
	 * @return void
	 */
	public function clearFlashMessages ( $identifier = 'core.template.flashMessages' ) {
		$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');	
		$flashMessageService = $objectManager->get(\TYPO3\CMS\Core\Messaging\FlashMessageService::class);
		$messageQueue = $flashMessageService->getMessageQueueByIdentifier( $identifier );
		$messageQueue->clear();
		$messageQueue->getAllMessagesAndFlush();
	}
	
	
	/**
	 *	Alle Flash-Messages rendern
	 *
	 * @var string 		$identifier
	 * @var boolean 	$returnArray
	 * @return string
	 */
	public function renderFlashMessages ( $identifier = 'core.template.flashMessages', $returnArray = false ) {

		$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');	
		$flashMessageService = $objectManager->get(\TYPO3\CMS\Core\Messaging\FlashMessageService::class);
		$messageQueue = $flashMessageService->getMessageQueueByIdentifier( $identifier );

		if ($flashMessages = $messageQueue->getAllMessages()) {
			if ($returnArray) {
				$messageQueue->getAllMessagesAndFlush();
				return $flashMessages;
			}
			
			$messageQueue->clear();
			return html_entity_decode(GeneralUtility::makeInstance(\TYPO3\CMS\Core\Messaging\FlashMessageRendererResolver::class)
				->resolve()
				->render($flashMessages));
		}
		return '';
	}
	

	// --------------------------------------------------------------------------------------------------------------------
	// Logging
	
	public function simpleLog ( $str, $id = 'nnezine', $num = 0 ) {
		if (!$GLOBALS['BE_USER']) $GLOBALS['BE_USER'] = $GLOBALS['TSFE']->initializeBackendUser();
		if ($GLOBALS['BE_USER']) {
			$GLOBALS['BE_USER']->simplelog($str, $id, $num);
		}
	}
	
	
	// --------------------------------------------------------------------------------------------------------------------
	// Logging
	
	public function trimExplode ( $del, $str ) {
		if (!trim($str)) return array();
		$str = explode($del, $str);
		foreach ($str as $k=>$v) $str[$k] = trim($v);
		return $str;
	}
	
	public function trimExplodeArray ( $arr ) {
		if ($arr === '') return array();
		if (!is_array($arr)) $arr = GeneralUtility::trimExplode(',', $arr);
		$final = array();
		foreach ($arr as $n) {
			if (trim($n) !== '') $final[] = $n;
		}
		return $final;
	}

	public function intExplodeArray ( $arr ) {
		if ($arr === '') return array();
		if (!is_array($arr)) $arr = GeneralUtility::trimExplode(',', $arr);
		$final = array();
		foreach ($arr as $n) {
			if (trim($n) !== '') $final[] = intval($n);
		}
		return $final;
	}
	
	// --------------------------------------------------------------------------------------------------------------------
	// Diverse Header-Funktionen
	
	
	/**
	 *	Zwingt den Browser, die Seite neu zu laden, wenn der Zurück-Button geklickt wurde.
	 *	Bei dem Quiz würde man sonst kein Update des Punkte-Standes sehen.
	 *
	 * @return void
	 */
	public function forceReloadForBackButton () {
		header("Cache-Control: no-store, must-revalidate, max-age=0");
		header("Pragma: no-cache");
		header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
	}
	
	
	/**
	 *	Weiterleitung über http-Header location:...
	 *
	 * @var string|int 	$pid
	 * @var array 		$vars
	 * @return void
	 */
	public static function httpRedirect ( $pid = null, $vars = [] ) {
		if (!$pid) $pid = $GLOBALS['TSFE']->id;		
		$pi = self::piBaseObj();
		$link = $pi->pi_getPageLink($pid, '', $vars); 
		$link = GeneralUtility::locationHeaderUrl($link); 
		header('Location: '.$link); 
		exit(); 
	}

	
}