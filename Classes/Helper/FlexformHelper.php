<?php

namespace Nng\Nnfaq\Helper;

use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;


class FlexformHelper {
	
	/**
	* @var \Nng\Nnfaq\Utilities\SettingsUtility
	* @inject
	*/
	protected $settingsUtility;
	
	/**
    * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
    */
	protected $objectManager;
	
	
	function __construct () {
		$this->objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$this->settingsUtility = $this->objectManager->get('Nng\Nnfaq\Utilities\SettingsUtility');
	}
	
	
	function insertFromSetup ( $config, $a = null ) {
		
		$selectedAction = false;
		
		if ($config['config']['respectControllerAction']) {
			$selectedAction = $config['flexParentDatabaseRow']['pi_flexform']['data']['sDEF']['lDEF']['switchableControllerActions']['vDEF']['0'];
			$selectedAction = GeneralUtility::trimExplode(';', $selectedAction);
			if (!$selectedAction) {
				$config['items'] = [['Bitte zuerst speichern!', '']];
				return $config;
			}
		}
		
		
		if ($tspath = $config['config']['tspath']) {
			$ts = $this->settingsUtility->getTsSetupFromTsPath( $tspath );
		} else {
			$ts = $this->settingsUtility->getTsSetup( false, 'tx_nnfaq_faq' );
		}
		
		if (!$ts) {
			$config['items'] = array_merge( $config['items'], [['Kein TS gefunden - Template-Vorlagen können per '.$tspath.' definiert werden', '']] );
			return $config;
		}
		
		foreach ($ts as $k=>$v) {
			if (is_array($v)) {
				$limitToAction = GeneralUtility::trimExplode(',', $v['controllerAction'], true);
				if ($limitToAction && $selectedAction) {
					
					if (array_intersect($limitToAction, $selectedAction)) {
						$config['items'] = array_merge( $config['items'], [[$v['label'], $k, '']] );
					}
				} else {
				
					$config['items'] = array_merge( $config['items'], [[$v['label'], $k, '']] );
				}
			}
		}
		
		return $config;
	}
	
}