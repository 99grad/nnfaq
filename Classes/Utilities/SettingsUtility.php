<?php
namespace Nng\Nnfaq\Utilities;

use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use \TYPO3\CMS\Core\Utility\ArrayUtility;
use \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use \TYPO3\CMS\Core\Database\ConnectionPool;

class SettingsUtility {

	/**
	 * @var \TYPO3\CMS\Extbase\Service\TypoScriptService
	 * @inject
	 */
	protected $typoscriptService;
	
	/**
	 * @var \TYPO3\CMS\Extbase\Service\FlexFormService
	 * @inject
	 */
	protected $flexFormService;

	/**
	 * @var array
	 */
	protected $settings;

	/**
	 * @var array
	 */	
	protected $mergedSettings;

	/**
	 * @var array
	 */	
	protected $configuration;
	
	/**
	 * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
	 */
 	protected $configurationManager;
 	
 	
 	/**
	 *	Stellt TypoScript + FlexForm Settings zur Verfügung
	 *
	 */
	public function injectConfigurationManager(ConfigurationManagerInterface $configurationManager) {
		$this->configurationManager = $configurationManager;
		$this->configuration = $this->configurationManager->getConfiguration( \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK );
  	}
  
	public function getMergedSettings () {
		if ($cache = $this->mergedSettings) return $cache;
  		$this->settings = $this->configurationManager->getConfiguration( \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS );
		$this->mergedSettings = $this->merge_settings_with_flexform();
		$this->mergedSettings['storagePid'] = $this->configuration['persistence']['storagePid'];
		return $this->mergedSettings;
	}
	
	public function getConfiguration () {
		return $this->configuration;
	}
	
		
	/**
	 * Merge flexform values with settings - only if flexform-value is not empty
	 *
	 */
	private function merge_settings_with_flexform () {

		if (!$this->settings) return array();
		$tmp = array_merge_recursive( $this->settings );
		if (!$this->settings['flexform']) return $tmp;
		
		self::mergeRecursiveWithOverrule( $tmp, $this->settings['flexform'] );
		return $tmp;
	}
	
	
	static public function mergeRecursiveWithOverrule(array &$original, array $overrule) {
		foreach (array_keys($overrule) as $key) {
			if (!isset($original[$key])) $original[$key] = $overrule[$key];
			if (is_array($original[$key])) {
				if (is_array($overrule[$key])) {
					self::mergeRecursiveWithOverrule($original[$key], $overrule[$key]);
				}
			} elseif (trim($overrule[$key])) {
				$original[$key] = $overrule[$key];
			}
		}
		reset($original);
	}

	/**
	*	Get paths to Templates / Partials from TypoScript Setup
	*
	*/	
	public function getTemplatePaths () {
		if (TYPO3_MODE == 'FE') {
			return $this->remove_ts_setup_dots( $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_nnfaq_faq.']['view.'] );
		}
		return [];
	}
	
	/**
	*	Get TypoScript Setup for TS-Path (i.e "plugin.tx_nnfaq_faq.predef")
	*
	*/
	public function getTsSetupFromTsPath ( $tsPath = '' ) {
	
		$parts = explode('.', $tsPath);
		$root = array_shift($parts);
		$plugin = array_shift($parts);
		$setup = $this->getTsSetup( false, $plugin, $root );
		
		if (!count($parts)) return $setup;
		
		while ($part = array_shift($parts)) {
			if (count($parts) == 0) {
				return is_array($setup[$part]) ? $setup[$part] : $setup[$part]; 
			}
			$setup = $setup[$part];
		}
		return $setup;
	}
	
    /**
	*	Get TypoScript Setup for plugin as array
	*
	*/
	public function getTsSetup ( $pageUid = false, $plugin = 'tx_nnfaq_faq', $root = 'plugin' ) {
		
		$pageUid = self::get_current_pid($pageUid);
		$cacheID = '__tsSetupCache_'.$pageUid.'_'.$plugin.'_'.$root;

		if ($GLOBALS[$cacheID]) return $GLOBALS[$cacheID];
		
		if (TYPO3_MODE == 'FE') {
			if (!$plugin) return $this->remove_ts_setup_dots($GLOBALS['TSFE']->tmpl->setup["{$root}."]);
			return $this->remove_ts_setup_dots($GLOBALS['TSFE']->tmpl->setup["{$root}."]["{$plugin}."]);
		}

		if (!$pageUid) echo 'SettingsUtility->getTsSetup() konnte pid nicht ermitteln.';
		
		$sysPageObj = GeneralUtility::makeInstance('TYPO3\CMS\Frontend\Page\PageRepository');
		$rootLine = $sysPageObj->getRootLine($pageUid);
		
		$TSObj = GeneralUtility::makeInstance('TYPO3\CMS\Core\TypoScript\ExtendedTemplateService');
		$TSObj->tt_track = 0;
		$TSObj->init();
		$TSObj->runThroughTemplates($rootLine);
		$TSObj->generateConfig();
		
		if (!$root) {
			return $this->remove_ts_setup_dots($TSObj->setup);
		}
		$GLOBALS[$cacheID] = $this->remove_ts_setup_dots(!$plugin ? $TSObj->setup["{$root}."] : $TSObj->setup["{$root}."]["{$plugin}."]);
		return $GLOBALS[$cacheID];		
	}
	
	
	public static function get_current_pid ( $pageUid = null ) {
		if (!$pageUid) $pageUid = (int) $GLOBALS['_REQUEST']['popViewId'];
		if (!$pageUid) $pageUid = (int) preg_replace( '/(.*)(id=)([0-9]*)(.*)/i', '\\3', $GLOBALS['_REQUEST']['returnUrl'] );
		if (!$pageUid) $pageUid = (int) preg_replace( '/(.*)(id=)([0-9]*)(.*)/i', '\\3', $GLOBALS['_POST']['returnUrl'] );
		if (!$pageUid) $pageUid = (int) preg_replace( '/(.*)(id=)([0-9]*)(.*)/i', '\\3', $GLOBALS['_GET']['returnUrl'] );
		if (!$pageUid && $_GET['edit']['pages']) $pageUid = (int) array_keys($_GET['edit']['pages'])[0];
		if (!$pageUid) $pageUid = (int) $GLOBALS['TSFE']->id;
		if (!$pageUid) $pageUid = (int) $_GET['id'];
		if (!$pageUid) {
			$pages = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecordsByField('pages', 'pid', 0);
			foreach ($pages as $p) {
				if ($p['is_siteroot']) {
					$page = $p;
					break;
				}
			}
    		$pageUid = intval($page['uid']);
		}
		return $pageUid;
	}
	
	
	public function getRootPidFromDomainRecord () {
	
		$domainStartPage = BackendUtility::getDomainStartPage($_SERVER['HTTP_HOST']);
		if ($pid = $domainStartPage['pid']) return $pid;

		$firstSysDomain = BackendUtility::getRecordRaw('sys_domain', 'hidden=0');
		$domainStartPage = BackendUtility::getDomainStartPage($firstSysDomain['domainName']);
		if ($pid = $domainStartPage['pid']) return $pid;
				
		$firstSysTemplate = BackendUtility::getRecordRaw('sys_template', 'hidden=0 AND deleted=0 AND root=1');
		if ($pid = $firstSysTemplate['pid']) return $pid;
		
		return false;
	}
	
	
	/**
	 *	Die uid einen tt_content-Elements wird übergeben (i.d.R. das Extension-PlugIn für diese Extension)
	 *	Rückgabe: Das TypoScript für diese Extension mit überlagerten settings-Werten aus dem FlexForm
	 *
	 */
	public function getSetupForPlugin ( $tt_content_uid ) {
		$data = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
			"*", 'tt_content', 
			'uid='.intval($tt_content_uid).' '.$this->getEnableFields( 'tt_content' ),
			'', '', '', ''
		);
		
		if (!$data) return array();		
		$ts = $this->remove_ts_setup_dots($this->getTsSetup());
		$flexform = $this->flexFormService->convertFlexFormContentToArray($data['pi_flexform']);
		$this->mergeRecursiveWithOverrule( $ts['settings'], $flexform['settings']['flexform'] );
		
		if ($pages = $data['pages']) $ts['persistence']['storagePid'] = $pages;		
		if ($recursive = $data['recursive']) $ts['persistence']['recursive'] = $recursive;		
		
		return $ts;
	}


		
	/* --------------------------------------------------------------- */
	
	/**
	 *	Aktuelle pageTSconfig für bestimmte Seite holen
	 *	z.B. um auf TCEFORM-Werte zugreifen zu können
	 */
	public function getPageTSconfig () {
		if (TYPO3_MODE == 'FE') {
			return $GLOBALS['TSFE']->pagesTSconfig;
		}
		return \TYPO3\CMS\Backend\Utility\BackendUtility::getPagesTSconfig($this->get_current_pid());
	}


	/* --------------------------------------------------------------- */
		
		
	public function getBaseURL () {
		$ts = $this->getTsSetup( false, true );
		if ($baseUrl = $ts['config']['baseURL']) return $baseUrl;
		$server = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://{$_SERVER[HTTP_HOST]}/";
		return $server;
	}
	
	public function getDomain () {
		$domain = preg_replace('/(http)([s]*)(:)\/\//i', '', $this->getBaseURL());
		if (substr($domain, -1) == '/') $domain = substr($domain, 0, -1);
		return $domain;
		
	}
	
	public function getAbsExtPath () {
		return ExtensionManagementUtility::extPath('nnfaq');
	}
	
	public function getRelExtPath () {
		return ExtensionManagementUtility::extRelPath('nnfaq');
	}
	
	public function getAbsTypo3Path () {
		return GeneralUtility::getIndpEnv('TYPO3_SITE_URL');
	}
	
	public function getPathSite () {
		return PATH_site;
	}
	
	public function getPathToFile( $file ) {
		if (file_exists($file)) return $file;
		if (file_exists(PATH_site.$file)) return PATH_site.$file;
		return false;
	}
	
	/**
	*	Prüfen, ob ein Feld im TCA existiert
	*
	*/
	public static function isTCAColumn ( $field, $table = 'tx_nnfaq_domain_model_question' ) {
		$cols = self::getTCAColumns( $table );
		return isset( $cols[$field] );
	}

	
	/**
	*	Aller TCA Felder holen für bestimmte Tabelle
	*
	*/
	public static function getTCAColumns ( $table = 'tx_nnfaq_domain_model_question' ) {
		$defaultCols = array('crdate', 'tstamp', 'pid', 'cruser_id');
		$cols = array_merge( $GLOBALS['TCA'][$table]['columns'], 
			array_combine( $defaultCols, $defaultCols ) 
		);
		foreach ($cols as $k=>$v) {
			$cols[GeneralUtility::underscoredToLowerCamelCase($k)] = $v;
		}
		return $cols;
	}
	
	/**
	*	Label eines bestimmten TCA Feldes holen
	*
	*/
	public static function getTCALabel ( $column = '', $table = 'tx_nnfaq_domain_model_question' ) {
		$tca = self::getTCAColumns( $table );
		$label = $tca[$column]['label'];
		if ($LL = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate($label)) return $LL;
		return $label;
	}
	
	
	public function getEnableFields ( $table ) {
		return $GLOBALS['TSFE']->sys_page->enableFields( $table, $GLOBALS['TSFE']->showHiddenRecords);
	}
	
	
	public function getExtConf ( $param = null, $ext = 'nnfaq' ) {
		$extConfig = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$ext]);
		return $param ? $extConfig[$param] : $extConfig;
	}
	
	
	/* --------------------------------------------------------------- 
		Wandelt die "."-Arrays eines TypoScripts um, damit z.B.
		per JSON oder Fluid darauf zugegriffen werden kann.		
	*/
	
	public function remove_ts_setup_dots ($ts) {
		if (!$ts) return array();
		return $this->typoscriptService->convertTypoScriptArrayToPlainArray($ts);		
	}
	
	public function add_ts_setup_dots ( $ts = array() ) {
		return $this->typoscriptService->convertPlainArrayToTypoScriptArray($ts);
	}
	
	
	function parse_flexform ( $xml, $sheetOnly='sDEF', $langOnly='lDEF' ) {
		if (!$xml) return array();
		$arr = GeneralUtility::xml2array($xml);
		$flat = array();
		foreach ($arr['data'] as $sheet=>$subArr) {
			foreach ($subArr as $k => $v) {
				$flat = ArrayUtility::arrayMergeRecursiveOverrule($flat, $v);
				//$flat[$k] = $v['vDEF'];
			}
		}
		
		foreach ($flat as $k=>$v) {
			$flat[$k] = $v['vDEF'];
		}
		
		return $flat;
	}
	
	
	// Macht aus "pages_36|Dateiarchiv" -> [36]
	
	function parse_pids ( $selRef ) {
		$selected_uids = array();
		if (!$selRef) return array();
		$itemArray = GeneralUtility::trimExplode(',',$selRef,1);
		if (is_array($itemArray)) {
			foreach ($itemArray as $kk => $vv) {
				$cuid = explode('|',$vv);
				$cuid = explode('_', $cuid[0]);
				$selected_uids[] = array_pop( $cuid );
			}
		}
		return $selected_uids;
	}
	
	/**
	* 	Holt eine Liste aller Seiten, die für einen Redakteur zugänglich sind.
	*	Berücksichtigt die Mount-Points und Zugriffsbeschränkungen auf die Seite.
	*	Abfrage kann nach modul (z.B. "faq") oder doktype eingeschränkt werden.
	*
	*/
	public function getAccessablePages ( $module = null, $doktype = null, $ignoreWebMounts = false ) {
	
		if (!$this->startBeUser()) return [];
		
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('pages');
		$queryBuilder->select('*')->from('pages')->orderBy('sorting');
		
		if ($module) {
			$queryBuilder->andWhere( $queryBuilder->expr()->eq('module', $queryBuilder->createNamedParameter($module)) );
		}
		if ($doktype) {
			$queryBuilder->andWhere( $queryBuilder->expr()->eq('doktype', $queryBuilder->createNamedParameter($doktype)) );
		}
		
		$pageRecords = $queryBuilder->execute()->fetchAll();

		if ($ignoreWebMounts) return $pageRecords;
		
		$accessable = [];
		foreach ($pageRecords as $page) {
			if ($GLOBALS['BE_USER']->doesUserHaveAccess($page, 1)) {
				$accessable[] = $page;
			}
		}
				
   		return $accessable;
	}
	
	/**
	* 	In manchen Fällen (Hooks) existiert kein BE_USER. Hier starten wir einen.
	*
	*/
	public function startBeUser () {
		// https://docs.typo3.org/typo3cms/CoreApiReference/ApiOverview/BackendUserObject/Index.html
		if (TYPO3_MODE !== 'BE') return false;

		if (!$GLOBALS['BE_USER']) {
			$GLOBALS['BE_USER'] = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Core\Authentication\BackendUserAuthentication');
			$GLOBALS['BE_USER']->start();
			$GLOBALS['BE_USER']->backendCheckLogin( true );
		}
		return $GLOBALS['BE_USER'];
	}
	
	/**
	* 	Holt eine Liste aller FAQ-Ordner, die für einen Redakteur bearbeitbar sind.
	*	Rückgabe: key = pids der SysFolder
	*/
	public function getAccessableFaqSysFolders ( $ignoreWebMounts = false ) {
					
		$pages = $this->getAccessablePages( 'nnfaq', null, $ignoreWebMounts );
		$results = [];
		foreach ($pages as $page) {
			$results[] = [
				'pid'				=> $page['uid'],
				'pageTitle'			=> $page['title'],
			];
		}
				
   		return $results;
	}
	
	
}