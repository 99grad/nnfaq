<?php
namespace Nng\Nnfaq\Domain\Model;

/***
 *
 * This file is part of the "nnfaq" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2016
 *
 ***/

/**
 * Question
 */
class Question extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * question
     *
     * @var string
     * @validate NotEmpty
     */
    protected $question = '';

	/**
     * @var string
     */
    protected $rating = '';
    
	/**
     * @var intval
     */
    protected $sorting = 0;
    
	
	/**
     * @var intval
     */
    protected $hidden = '';

    /**
     * answer
     *
     * @var string
     */
    protected $answer = '';

    /**
     * keywords
     *
     * @var string
     */
    protected $keywords = '';

	/**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     * @lazy
     */
    protected $media;
	   
 
    /**
     * Additional tt_content for Answer
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Nng\Nnfaq\Domain\Model\TtContent>
     * @lazy
     */
    protected $additionalContentAnswer;

    /**
     * categories
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Nng\Nnfaq\Domain\Model\Category>
     * @lazy
     */
    protected $categories = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->categories = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->additionalContentAnswer = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->media = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

	/**
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $media
	 */
	public function setMedia($media) {
		$this->media = $media;
	}

	/**
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
	 */
	public function getMedia() {
		return $this->media;
	}

	/**
	 * Add media
	 *
	 * @param FileReference $media
	 */
	public function addMedia(FileReference $media) {
		$this->media->attach($media);
	}

	/**
	 * Remove media
	 *
	 * @param FileReference $media
	 */
	public function removeMedia(FileReference $media) {
		$this->media->detach($media);
	}

	/**
	 * Get the first media
	 *
	 * @return FileReference|null
	 */
	public function getFirstMedia() {
		$media = $this->getMedia();
		if (!$media) return null;
		
		foreach ($media as $item) {
			return $item;
		}
		return null;
	}

   	/**
     * @return intval $hidden
     */
    public function getHidden() {
        return $this->hidden;
    }

    /**
     * @param intval $hidden
     * @return void
     */
    public function setHidden($hidden) {
        $this->hidden = $hidden;
    }

  	/**
     * @return intval $sorting
     */
    public function getSorting() {
        return $this->sorting;
    }

    /**
     * @param intval $sorting
     * @return void
     */
    public function setSorting($sorting) {
        $this->sorting = $sorting;
    }

  	/**
     * @return string $keywords
     */
    public function getKeywords() {
        return $this->keywords;
    }

    /**
     * @param string $keywords
     * @return void
     */
    public function setKeywords($keywords) {
        $this->keywords = $keywords;
    }

	/**
     * @return string $rating
     */
    public function getRating() {
        return $this->rating;
    }

    /**
     * @param string $rating
     * @return void
     */
    public function setRating($rating) {
        $this->rating = $rating;
    }

    /**
     * @return string $rating
     */
    public function getCategoryUids( $includeParents = false ) {
    	$catUids = [];
    	$categories = $this->getCategories();
    	foreach ($categories as $category) {
    		$uid = $category->getUid();
    		$catUids[$uid] = $uid;
    		if ($includeParents) {
    			$catUids += $category->getParentUids();
    		}
    	}
        return $catUids;
    }
    
    /**
     * Returns the question
     *
     * @return string $question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Sets the question
     *
     * @param string $question
     * @return void
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    }

    /**
     * Returns the answer
     *
     * @return string $answer
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Sets the answer
     *
     * @param string $answer
     * @return void
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    }

    /**
     * Get content elements (additionalContentAnswer)
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Nng\Nnfaq\Domain\Model\TtContent> $additionalContentAnswer
     */
    public function getAdditionalContentAnswer()
    {
        return $this->additionalContentAnswer;
    }

    /**
     * Get id list of content elements (additionalContentAnswer)
     *
     * @return string
     */
    public function getContentElementIdList()
    {
        $idList = [];
        $contentElements = $this->getAdditionalContentAnswer();
        if ($contentElements) {
            foreach ($this->getAdditionalContentAnswer() as $contentElement) {
                $idList[] = $contentElement->getUid();
            }
        }
        return implode(',', $idList);
    }

    /**
     * Adds a Category
     *
     * @param \Nng\Nnfaq\Domain\Model\Category $category
     * @return void
     */
    public function addCategory(\Nng\Nnfaq\Domain\Model\Category $category)
    {
        $this->categories->attach($category);
    }

    /**
     * Removes a Category
     *
     * @param \Nng\Nnfaq\Domain\Model\Category $categoryToRemove The Category to be removed
     * @return void
     */
    public function removeCategory(\Nng\Nnfaq\Domain\Model\Category $categoryToRemove)
    {
        $this->categories->detach($categoryToRemove);
    }

    /**
     * Returns the categories
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Nng\Nnfaq\Domain\Model\Category> $categories
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Sets the categories
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Nng\Nnfaq\Domain\Model\Category> $categories
     * @return void
     */
    public function setCategories(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $categories)
    {
        $this->categories = $categories;
    }
}
