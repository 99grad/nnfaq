<?php
namespace Nng\Nnfaq\Domain\Model;

/***
 *
 * This file is part of the "nnfaq" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2016
 *
 ***/

/**
 * Category
 */
class Category extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
  
	
	/**
     * @var string
     */
    protected $category = '';

	/**
     * @var intval
     */
    protected $position = 0;
    
    /**
     * @var intval
     */
    protected $hidden = 0;
    
    /**
     * @var intval
     */
    protected $uid;
 
    
   	/**
	 * Children
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Nng\Nnfaq\Domain\Model\Category>
	 * @lazy
	 */
	protected $children;
	
	/**
     * @var array
     */
    protected $childrenUids = [];
    
	/**
     * @var array
     */
    protected $parentUids = [];
    
    /**
     * @param intval $uid
     * @return void
     */
    public function setUid($uid) {
        $this->uid = $uid;
    }
    
  	/**
     * @return intval $position
     */
    public function getPosition() {
        return $this->position;
    }

    /**
     * @param intval $position
     * @return void
     */
    public function setPosition($position) {
        $this->position = $position;
    }

  	/**
     * @return intval $hidden
     */
    public function getHidden() {
        return $this->hidden;
    }

    /**
     * @param intval $hidden
     * @return void
     */
    public function setHidden($hidden) {
        $this->hidden = $hidden;
    }

    
  	/**
     * @return array $childrenUids
     */
    public function getChildrenUids() {
        if ($cache = $this->childrenUids) return $cache;
        $children = $this->getChildrenRecursive( $this );
        return $this->childrenUids = array_keys($children);
    }

	/**
     * @return array $parentUids
     */
    public function getParentUids() {
        if ($cache = $this->parentUids) return $cache;
        $uids = [];
        if ($this->getUid() > 0) {
			$parent = $this->getParent();
			while ($parent) {
				$uid = $parent->getUid();
				$uids[$uid] = $uid;
				$parent = $parent->getParent();
			}
		}
        return $this->childrenUids = $uids;
    }

	/**
     * @param array $children
     * @return void
     */
    public function getChildrenRecursive( $branch ) {
    	if (!isset($branch)) $branch = $this;
    	$children = [];
    	if ($branch && count($branch->getChildren())) {
    		foreach ($branch->getChildren() as $child) {
    			$uid = $child->getUid();
    			$children[$uid] = $child;
    			if ($subChildren = $this->getChildrenRecursive($child)) {
    				$children = $children + $subChildren;
    			}
    		}
    	}
    	return $children;
    }

    
    /**
     * @param array $childrenUids
     * @return void
     */
    public function setChildrenUids($childrenUids) {
        $this->childrenUids = $childrenUids;
    }
    
    
	/**
     * @param mixed $child
     * @return boolval
     */
    public function isParentOf( $child ) {
		if ($child && !is_numeric($child)) $child = $child->getUid();
		$childUids = $this->getChildrenUids();
		return isset($childUids[$child]);
    }
    
    /**
     * @param mixed $child
     * @return boolval
     */
    public function isChildOf( $parent ) {
		if ($parent && !is_numeric($parent)) $parent = $parent->getUid();
		$parentUids = $this->getParentUids();
		return isset($parentUids[$parent]);
    }
    
    
   	/**
	 * Parent
	 *
	 * @var \Nng\Nnfaq\Domain\Model\Category
	 * @lazy
	 */
	protected $parent;
    
    
	/**
     * @var string
     */
    protected $depth = '';
    
	
	/**
     * @var intval
     */
    protected $sorting = 0;
    
   
    /**
	 * Questions
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Nng\Nnfaq\Domain\Model\Question>
	 * @lazy
	 */
	protected $questions;

	
	/**
     * @return string $depth
     */
    public function getDepth() {
        return count($this->getParentUids());
    }

	
	
	/**
	 * Adds a Child
	 *
	 * @param \Nng\Nnfaq\Domain\Model\Category $child
	 * @return void
	 */
	public function addChild(\Nng\Nnfaq\Domain\Model\Category $child) {
		if (!$this->children) $this->children = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->children->attach($child);
	}
	
	/**
	 * Removes a Child
	 *
	 * @param \Nng\Nnfaq\Domain\Model\Category $child The Question to be removed
	 * @return void
	 */
	public function removeChild(\Nng\Nnfaq\Domain\Model\Category $child) {
		if (!$this->children) $this->children = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->children->detach($child);
	}
	
	/**
	 * Removes all children
	 *
	 * @return void
	 */
	public function removeAllChildren() {
		$this->children = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}
	
	
	/**
	 * Returns the children
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Nng\Nnfaq\Domain\Model\Category> $children
	 */
	public function getChildren() {
		return $this->children;
	}
	
	/**
	 * Adds a Question
	 *
	 * @param \Nng\Nnfaq\Domain\Model\Question $question
	 * @return void
	 */
	public function addQuestion(\Nng\Nnfaq\Domain\Model\Question $question) {
		$this->questions->attach($question);
	}

	/**
	 * Removes a Question
	 *
	 * @param \Nng\Nnfaq\Domain\Model\Question $questionToRemove The Question to be removed
	 * @return void
	 */
	public function removeQuestion(\Nng\Nnfaq\Domain\Model\Question $questionToRemove) {
		$this->questions->detach($questionToRemove);
	}
	
	/**
	 * Removes all questions
	 *
	 * @return void
	 */
	public function removeAllQuestions() {
		$this->questions = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the questions
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Nng\Nnfaq\Domain\Model\Question> $questions
	 */
	public function getQuestions() {
		return $this->questions;
	}

	/**
	 * Returns the total number questions, recursive
	 *
	 * @return intval $total
	 */
	public function getTotalQuestions() {
		$children = $this->getChildrenRecursive( $this );
		$total = count($this->getQuestions());
		foreach ($children as $child) {
			$total += count($child->getQuestions());
		}
		return $total;
	}

	/**
	 * Sets the questions
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Nng\Nnfaq\Domain\Model\Question> $questions
	 * @return void
	 */
	public function setQuestions(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $questions) {
		$this->questions = $questions;
	}
	
	
	/**
	 * Returns the parent
	 *
	 * @return \Nng\Nnfaq\Domain\Model\Category $parent
	 */
	public function getParent() {
		return $this->parent;
	}
	
	/**
	 * Sets the parent
	 *
	 * @param \Nng\Nnfaq\Domain\Model\Category $parent
	 * @return void
	 */
	public function setParent( $parent ) {
		$this->parent = $parent;
		$this->parentUids = [];
		$this->childrenUids = [];
	}
	
	/**
     *
     */
    public function __construct() {
        $this->questions = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->children = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }
     
  	/**
     * @return intval $sorting
     */
    public function getSorting() {
        return $this->sorting;
    }

    /**
     * @param intval $sorting
     * @return void
     */
    public function setSorting($sorting) {
        $this->sorting = $sorting;
    }


    /**
     * description
     *
     * @var string
     */
    protected $description = '';

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
    
    
  	/**
     * @return string $category
     */
    public function getCategory() {
        return $this->category;
    }

    /**
     * @param string $category
     * @return void
     */
    public function setCategory($category) {
        $this->category = $category;
    }

    
}
