<?php
namespace Nng\Nnfaq\Domain\Repository;

/***
 *
 * This file is part of the "nnfaq" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2016
 *
 ***/

/**
 * The repository for Categories
 */
class CategoryRepository extends \Nng\Nnfaq\Domain\Repository\AbstractRepository
{
    /**
     * @var array
     */
    protected $defaultOrderings = array(
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    );
    
    /**
     * @var \Nng\Nnfaq\Utilities\SettingsUtility
     * @inject
     */
    protected $settingsUtility = null;
    
    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     * @inject
     */
    protected $objectManager;
    
     /**
     * @var array
     */
    protected $cacheAllByUid = [];
    
    /**
     *	Gibt den Kategorien-Baum (hierarchisch) zurück
     *
     * @var array
     */
    public function getTree () {
    	$dataByUid = $this->getAllByUid();
		return [0=>$dataByUid[0]];
	}
	
    /**
     *	Gibt alle Branches des Kategorien-Baum zurück, key ist UID
     *
     * @var array
     */
    public function getAllByUid () {
    
    	if ($cache = $this->cacheAllByUid) return $cache;
    	
    	$data = $this->findAll();

    	$root = $this->objectManager->get('Nng\Nnfaq\Domain\Model\Category');
    	$root->setCategory('root');
    	
    	$dataByUid = [ 0=>$root ];
    	
    	// Array mit UID als Key generieren
    	foreach ($data as $category) {
    		$dataByUid[$category->getUid()] = $category;	
    	}

		// Baum generieren
    	foreach ($dataByUid as $uid=>&$category) {
    		if ($uid > 0) {
	    		$parent = $category->getParent();
	    		$parentUid = $parent && count($parent) ? $parent->getUid() : 0;
	    		if ($dataByUid[$parentUid]) {
					$dataByUid[$parentUid]->addChild( $category );
				}
	    	}
    	}
		
		return $this->cacheAllByUid = $dataByUid;
    }
    

	 /**
     *	Überschreibt findAll(), damit per FlexForm entschieden werden kann, ob hidden-Records geladen werden.
     *	Da es sich um IRRE-Relationen handelt, greift ->setIgnoreEnableFields( TRUE ) nicht.
     *
     * @var array
     */
    public function findAll () {

    	$settings = $this->settingsUtility->getMergedSettings();
    	$ignoreHidden = $settings['showHiddenCategories'] || $this->ignoreHidden;    	

		if (!$settings['storagePid']) {
			$this->ignoreStoragePid();
		}
		
    	$query = $this->createQuery();
    	if (!$ignoreHidden) {
	    	$query->matching(
	    		$query->equals('hidden', '0')
	    	);
	    }
	    return $query->execute();
	}
    
}
