<?php

namespace Nng\Nnfaq\Domain\Repository;


/**
 * Repository for tt_content objects
 *
 */
class TtContentRepository extends \Nng\Nnfaq\Domain\Repository\AbstractRepository
{
    protected $objectType = '\Nng\Nnfaq\Domain\Model\TtContent';
}
