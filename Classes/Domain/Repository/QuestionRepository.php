<?php
namespace Nng\Nnfaq\Domain\Repository;

use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;

/**
 * The repository for Questions
 */
class QuestionRepository extends \Nng\Nnfaq\Domain\Repository\AbstractRepository
{

	 /**
     * @var \Nng\Nnfaq\Utilities\SettingsUtility
     * @inject
     */
    protected $settingsUtility = null;
    
	 /**
     * @var \Nng\Nnfaq\Helper\AnyHelper
     * @inject
     */
    protected $anyHelper = null;
    
    /**
     * categoryRepository
     *
     * @var \Nng\Nnfaq\Domain\Repository\CategoryRepository
     * @inject
     */
    protected $categoryRepository = null;
    
    
    /**
     * @var array
     */
    protected $defaultOrderings = array(
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    );

	
    /**
     * Verwaiste Einträge finden
     *
     * @param array $settings
     * @return array|QueryResultInterface
     */
    public function findOrphans(array $settings = [])
    {
    	$this->ignoreStoragePid();
        $query = $this->createQuery();
		$results = $query->execute();
				
		// Waisen finden
		$orphans = [];
		foreach ($results as $result) {
			if (!count($result->getCategories())) $orphans[] = $result;
		}
		return $orphans;
	}
	
    /**
     * Find questions with constraints
     *
     * @param array $settings
     * @return array|QueryResultInterface
     */
    public function findQuestionsWithConstraints(array $settings = [])
    {
    	$this->ignoreStoragePid();
        $query = $this->createQuery();
		$constraints = [];

		$categoryTreeByUid = $this->categoryRepository->getAllByUid();

		// Nur diese Kategorien-uids zeigen
		$showCategories = $this->anyHelper->intExplodeArray($settings['selectCategory']);
		$showCategories = array_combine($showCategories, $showCategories);
		
		// Rekursive Unterkategorien ebenfalls berücksichtigen?
		if ($settings['recursive']) {
			foreach ($showCategories as $catUid) {
				if ($category = $categoryTreeByUid[$catUid]) {
					
					// Wurden ein oder mehrere Unterknoten einzeln gewählt, dann diese Unterkategorien nicht berücksichtigen
					$childrenUids = $category->getChildrenUids();
					$childrenUids = array_combine($childrenUids, $childrenUids);
					$anyChildWasSelected = count(array_intersect($childrenUids, $showCategories)) > 0;
					if (!$anyChildWasSelected) {
						$showCategories += $childrenUids;
					}
				}
			}	
		}

		// Beschränkung auf Top-Fragen?		
		if ($showPrio = $settings['showPrio']) {
			$constraints[] = $query->equals('rating', $showPrio);
		}

		// Beschränkung auf bestimmte Kategorien?
		if ($showCategories) {
			$tmp = [];
			foreach ($showCategories as $catUid) {
                $tmp[] = $query->contains('categories', $catUid);
            }
			$constraints[] = $query->logicalOr($tmp);
		}
		
		// Beschränkung auf Suchbegriff?
		if ($sword = $settings['sword']) {
			$tmp = [
				$query->like('question', "%{$sword}%"),
				$query->like('answer', "%{$sword}%"),
			];
			$constraints[] = $query->logicalOr($tmp);
		}
		
		if ($constraints) {
			$query->matching($query->logicalAnd($constraints));
		}

		$results = $query->execute();

		// "Flachen" Baum holen. Nur Kategorien, die Fragen enthalten zurückgeben?
		if ($settings['hideEmpty']) {
			$flatTree = $this->flattenResultTree( $results, $showCategories );
		} else {
			$flatTree = $this->flattenResultTree( false, $showCategories );		
		}

        return [
        	'orphans'				=> $orphans,
        	'questions' 			=> $results,
        	'flatTree'				=> $flatTree,
        	'questionsByCategories'	=> $this->groupQuestionsByCategory($results)
        ];
    }
    
     /**
     * Flacht den Kategorien-Baum ab:
     * Nicht gewählte Überkategorien werden rekursiv durchlaufen.
     * Falls Kinder gesetzt sind, werden sie hierarchisch nach oben gezogen.
     * Dadurch können im FlexForm einfach nur Unterknoten gewählt werden.
     *
     * @param array|QueryResultInterface $questions
     * @param array $selectedCategories
     * @return array
     */    
    public function flattenResultTree($questions = [], $selectedCategories = [] ) {
    
    	// Wenn Fragen übergeben wurden: Nur Kategorien berücksichtigen, zu denen es Fragen gibt
    	if ($questions) {
    		$categoriesInUse = [];

    		foreach ($questions as $question) {
    			$categoriesInUse += $question->getCategoryUids( true );
    		}
    		
    		// Im Backend keine Kategorien gewählt, aber Beschränkung auf Kategorien, die Fragen enthalten?
    		if (!$selectedCategories) {
    			$selectedCategories = $categoriesInUse;
    		} else {
	    		$selectedCategories = array_intersect($selectedCategories, $categoriesInUse);
	    		if (!$selectedCategories) return [];
	    	}
    	}
    	
    	$selectedCategories = array_combine($selectedCategories, $selectedCategories);
    	$categoryTree = $this->categoryRepository->getTree();
		$result = $this->flattenResultTreeRecursion($categoryTree, $selectedCategories);
		
		return $result;		
	}
		
	private function getTreeByUidRecursion( $branch = [] ) {
		$branchesByUid = [];
		foreach ($branch as &$category) {
			$catUid = intval($category->getUid());
			$branchesByUid[$catUid] = $category;
			$children = $category->getChildren();
			if (count($children) && ($subChildren = $this->getTreeByUidRecursion($children->toArray()))) {
				$branchesByUid += $subChildren;
			}
		}
		return $branchesByUid;
	}
	
	private function flattenResultTreeRecursion ( $branch, $selectedCategories = [] ) {
		$flatTree = new \Nng\Nnfaq\Domain\Model\CategorySearchResult();
		foreach ($branch as $category) {
			$catUid = $category->getUid();
			
			$subCategory = new \Nng\Nnfaq\Domain\Model\CategorySearchResult();
			$subCategory->setUid($catUid);
			$subCategory->setCategory($category->getCategory());
			$subCategory->setQuestions($category->getQuestions());
			$subCategory->setParent($category);
			
			$isSelectedCategory = isset($selectedCategories[$catUid]) || !$catUid || !$selectedCategories;
			
			if ($isSelectedCategory) {
				$flatTree->addChild($subCategory);
			}
			
			if ($children = $this->flattenResultTreeRecursion($category->getChildren(), $selectedCategories)) {
				$ref = $isSelectedCategory ? $subCategory : $flatTree;
				foreach ($children as $child) {
					$ref->addChild( $child );
				}
			}
		}
		return $flatTree->getChildren()->toArray();
	}
	
	
     /**
     * Gruppiert die Fragen nach Kategorien
     *
     * @param array|QueryResultInterface $questions
     * @return array
     */    
    public function groupQuestionsByCategory ($questions = []) {
    	$treeByCategoryUid = [];
		foreach ($questions as $question) {
			$categories = $question->getCategories()->toArray();
			if (count($categories)) {
				foreach ($categories as $category) {
					$categoryUid = $category->getUid();
					if (!$treeByCategoryUid[$categoryUid]) {
						$searchResult = new \Nng\Nnfaq\Domain\Model\CategorySearchResult();
						$searchResult->setUid( $category->getUid() );
						$searchResult->setCategory( $category->getCategory() );
						$searchResult->removeAllQuestions();
						$treeByCategoryUid[$categoryUid] = $searchResult;
					}
					$treeByCategoryUid[$categoryUid]->addQuestion( $question );
				}
			}
		}
		return $treeByCategoryUid;
    }
    
    
}
