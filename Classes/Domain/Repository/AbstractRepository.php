<?php
namespace Nng\Nnfaq\Domain\Repository;


/**
 * The repository abstraction
 */
class AbstractRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

	 /**
     * @var \Nng\Nnfaq\Utilities\SettingsUtility
     * @inject
     */
    protected $settingsUtility = null;
    
	 /**
     * @var \Nng\Nnfaq\Helper\AnyHelper
     * @inject
     */
    protected $anyHelper = null;

	
    protected $ignoreHidden = false;
    
    
    /**
     * @var array
     */
    protected $defaultOrderings = array(
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    );
    
     /**
     * 	Set contraints to get all entries, ignoring disabled items (i.e for backend-view)
     *
     */
    public function ignoreHidden()
    {
    	$query = $this->createQuery();
		$querySettings = $query->getQuerySettings();
		$querySettings->setIncludeDeleted( FALSE )
				->setRespectStoragePage( FALSE )
				->setIgnoreEnableFields( TRUE )
				->setEnableFieldsToBeIgnored(['hidden']);
				
		$this->setDefaultQuerySettings($querySettings);
		$this->ignoreHidden = true;
    }

     /**
     * 	Set contraints to get all entries, ignoring disabled items (i.e for backend-view)
     *
     */
    public function ignoreStoragePid()
    {
    	$query = $this->createQuery();
    	$querySettings = $query->getQuerySettings();
		$querySettings->setRespectStoragePage( FALSE );				
		$this->setDefaultQuerySettings($querySettings);			
    }
    
     /**
     * 	Set contraints to certain storagePid
     *
     */
    public function setStoragePid( $pidList = [] )
    {
    	if (!is_array($pidList)) $pidList = [$pidList];
    	$query = $this->createQuery();
    	$querySettings = $query->getQuerySettings();
		$querySettings->setRespectStoragePage( TRUE );				
		$querySettings->setStoragePageIds( $pidList );				
		$this->setDefaultQuerySettings($querySettings);			
    }
    
     /**
     * 	Override findByUid: 
     *	ignoreHidden() was ignored in normal query
     *
     */
    public function findByUid ( $uid ) {
    	$query = $this->createQuery();
		$query->matching($query->equals('uid', $uid));
        return $query->execute()->getFirst();
    }
    
}
