<?php

if (is_array($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'])) {
die("OK");
	$fixedPostVars = [];
	$postVarsSets = [
		'faq' => [
			'action' => [
				'GETvar' => 'tx_nnfaq_faq[action]',
				'valueMap' => ['c' => 'categories',],
				// 'noMatch' 	=> 'bypass',
			],
			'controller' => [
				'GETvar' => 'tx_nnfaq_faq[controller]',
				'valueMap' => ['n' => 'Question',],
				'noMatch' 	=> 'bypass',
			],
			'category' => [
				'GETvar' 		=> 'tx_nnfaq_faq[category]',
				//'noMatch' => 'bypass',
				/*
				'lookUpTable' 	=> [
					'table' 				=> 'tx_nnfaq_domain_model_category',
					'id_field' 				=> 'uid',
					'alias_field' 			=> "category",
					// 'addWhereClause' 		=> ' AND NOT deleted',
					'useUniqueCache' 		=> 1,
					'useUniqueCache_conf' 	=> [
						'strtolower' 		=> 	1,
						'spaceCharacter' 	=> 	'-',
					],
				],
				*/
			],
			'question' => [
				'GETvar' 		=> 'tx_nnfaq_faq[question]',
				//'noMatch' => 'bypass',
				/*
				'lookUpTable' 	=> [
					'table' 				=> 'tx_nnfaq_domain_model_question',
					'id_field' 				=> 'uid',
					'alias_field' 			=> "question",
					'useUniqueCache' 		=> 1,
					'useUniqueCache_conf' 	=> [
						'strtolower' 		=> 	1,
						'spaceCharacter' 	=> 	'-',
					],
				],
				*/
			],
			
		],
	];
	
	foreach ($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'] as $domain => $config) {
		if (is_array($config)) {
			//$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'][$domain]['fixedPostVars'] += $fixedPostVars;
			$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'][$domain]['postVarSets']['_DEFAULT'] += $postVarsSets;
		}
		unset($config);
	}
	
	//$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['fixedPostVars'] += $fixedPostVars;
	$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['postVarSets'] += $postVarsSets;

	unset($register);
	
	reset($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']);
}

