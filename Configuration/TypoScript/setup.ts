
plugin.tx_nnfaq_faq {
    view {
        templateRootPaths.0 = EXT:nnfaq/Resources/Private/Templates/
        partialRootPaths.0 = EXT:nnfaq/Resources/Private/Partials/
        layoutRootPaths.0 = EXT:nnfaq/Resources/Private/Layouts/
    }

	settings {
		templates {
			default {
				label = Nur Fragen zeigen, kein Kategorien-Filter
				
				# Wenn "controllerAction" angegeben wird, erscheint diese Auswahl im FF nur bei dieser Controller-Action
				#controllerAction = Category->list
				
				# Pfade können ergänzt / priorisiert werden mit Fallback auf plugin.tx_nnfaq_faq.view
				#templateRootPaths.10 = EXT:nnfaq/Resources/Private/Templates/
				#partialRootPaths.10 = EXT:nnfaq/Resources/Private/Partials/
				#layoutRootPaths.10 = EXT:nnfaq/Resources/Private/Layouts/

				# Andere Template-Datei verwenden?
				template = QuestionsOnly.html
			}
		}
	}
			
    persistence {
        storagePid >
        recursive >
    }
    
    features {
		skipDefaultArguments = 1
    }
}

page {
    includeJSFooter {
        tx_nnfaq = EXT:nnfaq/Resources/Public/Js/nnfaq.js
    }
    includeCSS {
        tx_nnfaq = EXT:nnfaq/Resources/Public/Css/nnfaq.css
    }
}


# Mapping tt_content
config.tx_extbase {
    persistence {
        enableAutomaticCacheClearing = 1
        updateReferenceIndex = 0
        classes {
            Nng\Nnfaq\Domain\Model\TtContent {
                mapping {
                    tableName = tt_content
                    columns {
                        uid.mapOnProperty = uid
                        pid.mapOnProperty = pid
                        sorting.mapOnProperty = sorting
                        CType.mapOnProperty = contentType
                        header.mapOnProperty = header
                    }
                }
            }
            Nng\Nnfaq\Domain\Model\Category {
                mapping {
                    tableName = tx_nnfaq_domain_model_category
                    columns {
                        sorting.mapOnProperty = sorting
                        hidden.mapOnProperty = hidden
                    }
                }
            }
            Nng\Nnfaq\Domain\Model\Question {
                mapping {
                    tableName = tx_nnfaq_domain_model_question
                    columns {
                        sorting.mapOnProperty = sorting
                        hidden.mapOnProperty = hidden
                    }
                }
            }
        }
    }
}

# Rendering of content elements in answer
lib.tx_nnfaq.contentElementRendering = RECORDS
lib.tx_nnfaq.contentElementRendering {
    tables = tt_content
    source.current = 1
    dontCheckPid = 1
}

## ----------------------------------------------------------------------------
## Module configuration
## ----------------------------------------------------------------------------

module.tx_nnfaq {
	persistence {
		storagePid = 
	}
	view {
		templateRootPaths {
			10 = EXT:nnfaq/Resources/Backend/Templates/
		}
		partialRootPaths { 
			10 = EXT:nnfaq/Resources/Backend/Partials/
		}
		layoutRootPaths {
			10 = EXT:nnfaq/Resources/Backend/Layouts/
		}
	}
}


