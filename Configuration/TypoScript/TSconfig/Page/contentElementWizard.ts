mod.wizards.newContentElement.wizardItems.plugins {
    elements {
        nnfaq {
            iconIdentifier = ext-nnfaq-wizard-icon
            title = LLL:EXT:nnfaq/Resources/Private/Language/locallang_db.xlf:pi1_title
            description = LLL:EXT:nnfaq/Resources/Private/Language/locallang_db.xlf:pi1_plus_wiz_description
            tt_content_defValues {
                CType = list
                list_type = nnfaq_faq
            }
        }
    }
}