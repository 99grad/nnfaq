<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:nnfaq/Resources/Private/Language/locallang_db.xlf:tx_nnfaq_domain_model_category',
        'label' => 'category',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => 1,
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            //'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'category,description,',
        'typeicon_classes' => [
            'default' => 'mimetypes-x-tx_nnfaq_domain_model_category'
        ],
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, category, description, questions',
    ],
    'types' => [
        '1' => ['showitem' => '
        	sys_language_uid, l10n_parent, l10n_diffsource, hidden, category, description, parent,
        	--div--;LLL:EXT:nnfaq/Resources/Private/Language/locallang_db.xlf:tx_nnfaq_domain_model_category.questions,questions, 
        	--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime
        '],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages'
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_nnfaq_domain_model_category',
                'foreign_table_where' => 'AND tx_nnfaq_domain_model_category.pid=###CURRENT_PID### AND tx_nnfaq_domain_model_category.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime',
                'range' => [
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ],
                'allowLanguageSynchronization' => true,
            ],
        ],
        'endtime' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime',
                'range' => [
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ],
                'allowLanguageSynchronization' => true,
            ],
        ],
        'questions' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:nnfaq/Resources/Private/Language/locallang_db.xlf:tx_nnfaq_domain_model_question',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_nnfaq_domain_model_question',
                'MM' => 'tx_nnfaq_question_category_mm',
                'MM_opposite_field' => 'category',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'enableMultiSelectFilterTextfield' => true,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                ],
            ],
        ],
        'category' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:nnfaq/Resources/Private/Language/locallang_db.xlf:tx_nnfaq_domain_model_category.category',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'parent' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:nnfaq/Resources/Private/Language/locallang_db.xlf:tx_nnfaq_domain_model_category.parent',
            'config' => [
                'type' => 'input',
                'renderType' => 'nnfaqCategoryTree',
                'maxItems' => 1,
            ],

        ],
        'description' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:nnfaq/Resources/Private/Language/locallang_db.xlf:tx_nnfaq_domain_model_category.description',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ]

        ],

    ],
];
