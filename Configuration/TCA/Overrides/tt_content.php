<?php
defined('TYPO3_MODE') or die();

call_user_func(
    function ($extKey) {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Nng.' . $extKey,
            'Faq',
            'nnfaq'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extKey, 'Configuration/TypoScript', 'nnfaq');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_nnfaq_domain_model_question',
            'EXT:nnfaq/Resources/Private/Language/locallang_csh_tx_nnfaq_domain_model_question.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_nnfaq_domain_model_question');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_nnfaq_domain_model_category',
            'EXT:nnfaq/Resources/Private/Language/locallang_csh_tx_nnfaq_domain_model_category.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_nnfaq_domain_model_category');

        $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['nnfaq_faq'] = 'recursive,select_key';
    
        $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['nnfaq_faq'] = 'pi_flexform';
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
            'nnfaq_faq',
            'FILE:EXT:' . $extKey . '/Configuration/FlexForm/Flexform.xml'
        );
    
        $GLOBALS['TCA']['pages']['columns']['module']['config']['items'][] = array('nnfaq', $extKey, 'apps-pagetree-folder-contains-board');
        
        $GLOBALS['TCA']['pages']['ctrl']['typeicon_classes']['contains-nnfaq'] = 'apps-pagetree-folder-contains-board';
    },
    'nnfaq'
);
